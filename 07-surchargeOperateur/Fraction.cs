﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_surchargeOperateur
{

    // Exercice Surcharge opérateur
    // - + entre 2 Fractions(utiliser la méthode pgcd pour simplifier la fraction)
    // - *  entre 2 Fractions(utiliser la méthode pgcd pour simplifier la fraction)
    // - *  entre une Fractions et un entier(utiliser la méthode pgcd pour simplifier la fraction)
    // - == et != les opérateurs retournent un booléen et prend en paramètre 2 Fractions
    // - tostring
    internal class Fraction
    {
        private int Numerateur { get; set; } = 0;

        private int _denominateur = 1;

        public int Denominateur
        {
            get => _denominateur;
            set
            {
                if (value == 0)
                {
                    throw new ArgumentException("Denominateur à 0");
                }
                _denominateur = value;
            }
        }
        public Fraction(int numerateur, int denominateur)
        {
            Numerateur = numerateur;
            if (denominateur == 0)
            {
                throw new ArgumentException("Denominateur à 0");
            }
            _denominateur = denominateur;
        }

        public double Calculer()
        {
            return ((double)Numerateur) / _denominateur;
        }



        public static Fraction operator +(Fraction f1, Fraction f2)
        {
            int n = f2.Numerateur * f1._denominateur + f1.Numerateur * f2._denominateur;
            int d = f1._denominateur * f2._denominateur;
            Simplifier(ref n, ref d);
            return new Fraction(n, d);
        }

        public static Fraction operator *(Fraction f1, Fraction f2)
        {
            int n = f1.Numerateur * f2.Numerateur;
            int d = f1._denominateur * f2._denominateur;
            Simplifier(ref n, ref d);
            return new Fraction(n, d);
        }

        public static Fraction operator *(Fraction f, int v)
        {
            int n = f.Numerateur * v;
            int d = f._denominateur;
            Simplifier(ref n, ref d);
            return new Fraction(n, d);
        }

        public static bool operator ==(Fraction f1, Fraction f2)
        {
            int n1 = f1.Numerateur;
            int d1 = f2._denominateur;
            Simplifier(ref n1, ref d1);
            int n2 = f2.Numerateur;
            int d2 = f2._denominateur;
            Simplifier(ref n2, ref d2);
            return n1 == n2 && d1 == d2;
        }
        public static bool operator !=(Fraction f1, Fraction f2)
        {
            return !(f1 == f2);
        }

        private static void Simplifier(ref int num, ref int den)
        {
            int p = Pgcd(num, den);
            num /= p;
            den /= p;
        }

        private static int Pgcd(int a, int b)
        {
            while (a != b)
            {
                if (a > b)
                {
                    a -= b;
                }
                else
                {
                    b -= a;
                }
            }
            return a;
        }



        public override string ToString()
        {
            return _denominateur != 1 ? $"{Numerateur}/{_denominateur}" : $"{Numerateur}";
        }

    }
}
