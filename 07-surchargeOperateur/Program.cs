﻿

namespace _07_surchargeOperateur
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Point a = new Point(1, 2);
            Point b = -a;
            Console.WriteLine(b);
            Point c = new Point(2, 3);
            Point d = a + c;
            Console.WriteLine(d);
            d += a; // d = d+a // assignation composé implicite
            Console.WriteLine(d);
            Point e = a * 5;
            Console.WriteLine(e);
            a *= 2; // assignation composé implicite
            Console.WriteLine(a);

            #region Exercicr fraction
            Fraction fa = new Fraction(1, 2);
            Fraction fb = new Fraction(2, 8);
            Console.WriteLine($"{fa.Calculer()}   {fb.Calculer()}");
            Fraction rs1 = fa + fb;
            Console.WriteLine(rs1);

            Fraction rs2 = fa * fb;
            Console.WriteLine(rs2);

            Fraction rs3 = fa * 2;
            Console.WriteLine(rs3);

            Console.WriteLine(fa == fb);
            Console.WriteLine(fa == fb * 2);
            Console.WriteLine(fa != fb);

            #endregion

        }
    }
}
