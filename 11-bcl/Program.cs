﻿using System.Collections;
using System.Net.Http.Headers;
using System.Text;

namespace _11_bcl
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region chaine de caractère
            string hello = "hello";
            string str = new string('a', 10);
            Console.WriteLine($"{hello} {str}");
            Console.WriteLine("bonjour".ToUpper());

            // Length -> Nombre de caractère de la chaine de caractère
            Console.WriteLine(hello.Length);    // 10

            // Concaténation 
            // avec l'opérateur + 
            string str2 = hello + "World";
            Console.WriteLine(str2);
            // ou avec la méthode de classe Concat
            string str3 = string.Concat(hello, " world");
            Console.WriteLine(str3);

            // La méthode Join concatène les chaines, en les séparants par une chaine de séparation
            string str4 = string.Join(";", "ady", "rty", "hkhkhg", "bcbc");
            Console.WriteLine(str4);

            // Découpe la chaine en un tableau de sous-chaine suivant un ou plusieurs séparateur, par défaut le caractère espace
            string[] tabStr = str4.Split(';');
            foreach (string s in tabStr)
            {
                Console.WriteLine(s);
            }

            // Substring permet d'extraire une sous-chaine d'une chaine
            // de l'indice passé en paramètre juqu'à la fin de la chaine
            Console.WriteLine(str3.Substring(6));
            // ou pour le nombre de caractères passé en paramètre
            Console.WriteLine(str3.Substring(6, 2));

            // Insérer une sous-chaine à partir d'un indice
            Console.WriteLine(str3.Insert(5, "------"));

            // Supprimer un nombre de caractère à partir d'un indice
            Console.WriteLine(str3.Remove(5, 4));

            // StartsWith retourne true si la chaine commence par la chaine passé en paramètre
            Console.WriteLine(str3.StartsWith("Hell")); // true
            Console.WriteLine(str3.StartsWith("aaa")); //false
                                                       // IndexOf retourne la première occurence du caractère ou de la chaine passée en paramètre    
            Console.WriteLine(str3.IndexOf("o")); // 4
            Console.WriteLine(str3.IndexOf("o", 5)); // 7   idem mais à partir de l'indice passé en paramètre
            Console.WriteLine(str3.IndexOf("o", 8)); // -1  -1 retourne -1, si le caractère n'est pas trouvé

            // Remplace toutes les chaines (ou caratères) oldValue par newValue
            Console.WriteLine(str3.Replace('o', 'a'));

            // Retourne True si la sous-chaine passée en paramètre est contenu dans la chaine
            Console.WriteLine(str3.Contains("wo")); // true
            Console.WriteLine(str3.Contains("aaa")); // false

            // Trim supprime les caractères de blanc du début et de la fin de la chaine
            Console.WriteLine("   \n \t\t Hello World    \n\n\n\t\t".Trim());
            Console.WriteLine("   \n \t\t Hello World    \n\n\n\t\t".TrimStart());  // idem mais uniquement au début de la chaine
            Console.WriteLine("   \n \t\t Hello World    \n\n\n\t\t".TrimEnd());    // idem mais uniquement à la fin de la chaine

            // Aligne les caractères à gauche en les complétant par un caractère (par défaut ' ') à gauche pour une longueur spécifiée
            Console.WriteLine(str3.PadLeft(50));
            Console.WriteLine(str3.PadLeft(50, '_'));
            // Aligne les caractères à droite en les complétant par un caractère (par défaut ' ') à droite pour une longueur spécifiée
            Console.WriteLine(str3.PadRight(50, '_'));

            // ToUpper convertie tous les caractères en majuscule
            Console.WriteLine(str3.ToUpper());

            // ToLower convertie tous les caractères en minuscule
            Console.WriteLine(str3.ToLower());

            Console.WriteLine(str3[3]);
            //str3[3] = 'c';

            // égalité de 2 chaines  == ou equals
            Console.WriteLine("Hello".Equals(hello));
            Console.WriteLine("Hello" == hello);

            // Comparaison 0-> égale, 1-> se trouve après, -1 -> se trouve avant dans l'ordre alphabétique
            // Il existe 2 méthodes: une d'instance et de classe
            Console.WriteLine(string.Compare(hello, "bonjour")); //1
            Console.WriteLine("bonjour".CompareTo(hello)); //-1

            // On peut chainer l'appel des différentes méthodes
            Console.WriteLine(str3.ToLower().Trim().Substring(6).ToUpper());

            // StringBuilder
            // Lorsqu'il y a beaucoup de manipulation (+de 3) sur une chaine ( concaténation et insertion, remplacement,supression de sous-chaine)
            // il vaut mieux utiliser un objet StringBuilder qui est plus performant qu'un objet String => pas de création d'objet intermédiare
            StringBuilder sb = new StringBuilder("azerty");
            sb.Append("______");
            sb.Append(123);
            sb.Remove(4, 2);
            string str7 = sb.ToString();
            Console.WriteLine(str7);
            #endregion

            #region Exercices chaine de caractères
            // Inverser
            Console.WriteLine(Inverser("Bonjour"));
            // Palindrome
            Console.WriteLine(Palidrome("Radar"));
            Console.WriteLine(Palidrome("Bonjour"));
            // Accronyme
            Console.WriteLine(Acronyme("Comité  international olympique"));
            Console.WriteLine(Acronyme("Organisation du traité de l'Atlantique Nord"));
            #endregion

            #region DateTime
            DateTime aujoudhui = DateTime.Now;// DateTime.Now => récupérer l'heure et la date courante
            Console.WriteLine(aujoudhui);

            Console.WriteLine(aujoudhui.Year);
            Console.WriteLine(aujoudhui.Day);
            Console.WriteLine(aujoudhui.DayOfYear);


            Console.WriteLine(DateTime.UtcNow);

            DateTime vacance = new DateTime(2024, 8, 1);
            Console.WriteLine(vacance);

            // TimeSpan => représente une durée
            TimeSpan duree = vacance - aujoudhui;
            Console.WriteLine(duree);
            Console.WriteLine(duree.TotalHours);
            Console.WriteLine(duree.Days);

            TimeSpan dixJour = new TimeSpan(10, 0, 0, 0);
            Console.WriteLine(aujoudhui + dixJour);
            Console.WriteLine(aujoudhui.Add(dixJour));
            Console.WriteLine(aujoudhui.AddMonths(3));

            Console.WriteLine(DateTime.Compare(vacance, aujoudhui)); // -1
            Console.WriteLine(aujoudhui.CompareTo(vacance)); //1
            Console.WriteLine(aujoudhui > vacance);

            // DateTime -> string
            Console.WriteLine(aujoudhui.ToLongDateString());
            Console.WriteLine(aujoudhui.ToLongTimeString());
            Console.WriteLine(aujoudhui.ToShortDateString());
            Console.WriteLine(aujoudhui.ToShortTimeString());

            Console.WriteLine(aujoudhui.ToString("ddd MM yyyy"));

            // string -> DateTime
            DateTime d2 = DateTime.Parse("2024/04/20T11:30:00");
            Console.WriteLine(d2);
            #endregion

            #region collection

            #region Collection faiblement typé
            // C# 1 => elle peut contenir tous types d'objets
            ArrayList lst = new ArrayList();
            lst.Add("Hello");
            lst.Add("World)");
            lst.Add(12);
            lst.Add(12.4);
            lst[0] = "Bonjour";
            Console.WriteLine(lst.Count);
            if (lst[0] is string str8)
            {
                Console.WriteLine(str8);
            }
            #endregion

            #region  Collection fortement typée
            //à partir de C# 2 => type générique
            List<string> lstStr = new List<string>();
            lstStr.Add("hello");
            //lstStr.Add(12);   // On ne peut plus qu'ajouter des chaines de caractères => sinon erreur de complilation
            lstStr.Add("World");
            lstStr.Add("azerty");
            lstStr.Add("a supprimer");

            Console.WriteLine(lstStr[0]);   // Accès à un élément de la liste
            lstStr[0] = "Bonjour";

            Console.WriteLine(lstStr.Count);    // Nombre d'élément de la collection
            Console.WriteLine(lstStr.Max());    // Valeur maximum stocké dans la liste
            Console.WriteLine(lstStr.Min());    // Inverser l'ordre de la liste

            lstStr.RemoveAt(3);

            // Parcourir la collection complétement
            foreach (var s in lstStr) // à partir de c#2
            {
                Console.WriteLine(s);
            }

            lstStr.Reverse();   // Inverser l'ordre de la liste

            // Parcourrir un collection avec un Enumérateur
            var it = lstStr.GetEnumerator();
            while (it.MoveNext())
            {
                Console.WriteLine(it.Current);
            }

            // IEnumerable
            var it2 = NombrePaire().GetEnumerator();
            it2.MoveNext();
            Console.WriteLine(it2.Current);
            foreach (var v in NombrePaire())
            {
                Console.WriteLine(v);
            }

            // IEnumerable -> Parcours d'une chaine
            string str9 = "Hello world";
            foreach (var c in str9)
            {
                Console.WriteLine(c);
            }
            // Dictionnary => association Clé / Valeur
            Dictionary<int, string> m = new Dictionary<int, string>();
            m.Add(123, "AZERTY");   // Add => ajout d'un valeur associé à une clé
            m.Add(12, "Hello");
            m.Add(345, "World");
            m.Add(56, "A supprimer ");
            // m.Add(56, "A supprimer "); // => On ne peut pas ajouter, si la clé éxiste déjà => exception

            // accès à un élément m[clé] => valeur
            Console.WriteLine(m[12]);
            m[123] = "bonjour"; // modifier une valeur associé à la clé
            m.Remove(56);

            Console.WriteLine(m.Count);

            // Parcourir un dictionnary
            foreach (var kv in m)
            {
                Console.WriteLine($"{kv.Key} {kv.Value}");
            }

            // Contains -> tester l'existance d'une clé ou d'un valeur
            Console.WriteLine(m.ContainsKey(123));
            Console.WriteLine(m.ContainsValue("Hello"));

            // Obtenir et parcourir toutes les clés du dictionary
            var keys = m.Keys;
            foreach (var k in keys)
            {
                Console.WriteLine(k);
            }

            // Stack => pile FILO
            Stack<int> st = new Stack<int>();
            st.Push(12);        // Ajouter un élément
            st.Push(132);
            st.Push(124);
            Console.WriteLine(st.Count);    // Nombre d'élément dans la pile
            Console.WriteLine(st.Peek());   // Peek => Lire l'élément en tête de la pile sans le retirer
            Console.WriteLine(st.Peek());

            Console.WriteLine(st.Pop());    // Pop => Lire l'élément en tête de la pile et le retire de la pile
            Console.WriteLine(st.Pop());
            #endregion
            #endregion

            #region type générique

            // Classe
            // Box<string> b1 = new Box<string>("hello");
            // Box<int> b2 = new Box<int>(3);
            Box<Article> b3 = new Box<Article>(new Article("tv", 600.0));

            // Méthode générique
            Afficher(2);     // Le type générique est déduit du type des paramètres => string
            Afficher("azerty");
            #endregion


        }

        #region Méthode générique
        public static void Afficher<T>(T a)
        {
            Console.WriteLine(a);
        }
        #endregion

        #region IEnumerable
        static IEnumerable<int> NombrePaire()
        {
            for (int i = 0; i < 100; i += 2)
            {
                yield return i;
            }
        }
        #endregion

        #region Exercice Inverser
        // Écrire la méthode Inverser qui prend en paramètre une chaine et qui retourne la chaine avec les caractères inversés
        // exemple : bonjour => ruojnob
        public static string Inverser(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = str.Length - 1; i >= 0; i--)
            {
                sb.Append(str[i]);
            }
            return sb.ToString();
        }
        #endregion

        #region Exercice Palidrome
        // Écrire une méthode qui accepte en paramètre une chaine et qui retourne un booléen pour indiquer si c'est palindrome
        // exemple :: SOS, radar
        public static bool Palidrome(string str)
        {
            string tmp = str.Trim().ToLower();
            return tmp == Inverser(tmp);
        }
        #endregion

        #region Exercice Acronyme
        // Faire une méthode de classe qui prend en paramètre une phrase et qui retourne un acronyme       
        // Les mots de la phrase sont pris en compte s'ils contiennent plus de 2 lettres

        // Ex: Comité international olympique → CIO
        //    Organisation du traité de l'Atlantique Nord → OTAN
        public static string Acronyme(string str)
        {
            StringBuilder sb = new StringBuilder();
            string[] words = str.Split(' ', '\'');
            foreach (var w in words)
            {
                if (w.Length > 2)
                {
                    sb.Append(w[0]);
                }
            }
            return sb.ToString().ToUpper();
        }
        #endregion
    }
}
