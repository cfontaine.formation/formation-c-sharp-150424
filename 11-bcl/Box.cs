﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11_bcl
{
    internal class Box<T> where T: BaseEntite
    {
        public T Value { get; set; }

        public Box(T value)
        {
            Value = value;
        }

        public bool IsEmpty()
        {
            return Value == null;
        }
    }
}
