﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11_bcl
{
    internal class Article : BaseEntite
    {
        public string Description { get; set; }

        public double Prix { get; set; }

        public Article(string description, double prix)
        {
            Description = description;
            Prix = prix;
        }
    }
}
