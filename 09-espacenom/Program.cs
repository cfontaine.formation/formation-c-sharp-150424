﻿using _09_espacenom.gui;


// Definir un Alias
using ConsoleSys = System.Console;


namespace _09_espacenom
{

    internal class Program
    {
        static void Main(string[] args)
        {
            // Nom complet d'une classe namespace.classe
            _09_espacenom.gui.Window window = new _09_espacenom.gui.Window();
            // avec using _09_espacenom.gui;
            Window win2 = new Window();

            // En cas de conflit de nom, il faut utiliser le nom complet des classe
            _09_espacenom.gui.Console c = new _09_espacenom.gui.Console();
            System.Console.WriteLine("Hello");

            // ou un Alias 
            ConsoleSys.WriteLine();
        }
    }
}
