﻿using _12_Libfichier;
namespace _12_Fichier
{
    // Pour ajouter au projet la bibliothèque
    // Cliquer droit sur le projet -> Ajouter -> référence -> choisir 13-LibFichier

    internal class Program
    {
        static void Main(string[] args)
        {
            Fichier.InfoLecteur();
            Fichier.Parcourir(@"C:\Dawan\Formations\spring boot");
            Fichier.InfoFichier(@"C:\Dawan\Temp\test.txt");
            List<Article> list = new List<Article>();
            list.Add(new Article("TV 4k", 450.0, "Tv"));
            list.Add(new Article("TV hd", 250.0, "Tv"));
            list.Add(new Article("smartphone android", 450.0, "Téléphone"));
            Fichier.ArticlesToCsv(list, @"C:\Dawan\Formations\articles.csv");

            List<Article> articles = Fichier.ArticleFromCsv(@"C:\Dawan\Formations\articles.csv");
            foreach (var a in articles)
            {
                Console.WriteLine(a);
            }
            Fichier.EcrireFichierBinaire(@"C:\Dawan\Formation\Test.bin");
            Fichier.LireFichierBinaire(@"C:\Dawan\Formation\Test.bin");

            Fichier.Copie(@"C:\Dawan\Formation\Logo dawan.png", @"C:\Dawan\Formation\test.png");

        }
    }
}
