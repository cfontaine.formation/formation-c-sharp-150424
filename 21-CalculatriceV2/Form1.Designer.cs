﻿namespace _21_CalculatriceV2
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            splitContainer1 = new SplitContainer();
            textBoxResultat = new TextBox();
            tableLayoutPanel1 = new TableLayoutPanel();
            buttonEgal = new Button();
            buttonVirgule = new Button();
            button0 = new Button();
            buttonSigne = new Button();
            buttonPlus = new Button();
            button3 = new Button();
            button2 = new Button();
            button1 = new Button();
            buttonMoins = new Button();
            button6 = new Button();
            button5 = new Button();
            button4 = new Button();
            buttonMultiplier = new Button();
            button9 = new Button();
            button8 = new Button();
            button7 = new Button();
            buttonDIvision = new Button();
            buttonBackspace = new Button();
            buttonC = new Button();
            buttonCE = new Button();
            ((System.ComponentModel.ISupportInitialize)splitContainer1).BeginInit();
            splitContainer1.Panel1.SuspendLayout();
            splitContainer1.Panel2.SuspendLayout();
            splitContainer1.SuspendLayout();
            tableLayoutPanel1.SuspendLayout();
            SuspendLayout();
            // 
            // splitContainer1
            // 
            splitContainer1.Dock = DockStyle.Fill;
            splitContainer1.Location = new Point(0, 0);
            splitContainer1.Margin = new Padding(3, 4, 3, 4);
            splitContainer1.Name = "splitContainer1";
            splitContainer1.Orientation = Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            splitContainer1.Panel1.Controls.Add(textBoxResultat);
            splitContainer1.Panel1.RightToLeft = RightToLeft.Yes;
            // 
            // splitContainer1.Panel2
            // 
            splitContainer1.Panel2.Controls.Add(tableLayoutPanel1);
            splitContainer1.Size = new Size(800, 562);
            splitContainer1.SplitterDistance = 154;
            splitContainer1.SplitterWidth = 5;
            splitContainer1.TabIndex = 0;
            // 
            // textBoxResultat
            // 
            textBoxResultat.BackColor = SystemColors.Control;
            textBoxResultat.BorderStyle = BorderStyle.None;
            textBoxResultat.Dock = DockStyle.Fill;
            textBoxResultat.Font = new Font("Microsoft Sans Serif", 36F, FontStyle.Regular, GraphicsUnit.Point, 0);
            textBoxResultat.Location = new Point(0, 0);
            textBoxResultat.Margin = new Padding(15, 19, 15, 19);
            textBoxResultat.Name = "textBoxResultat";
            textBoxResultat.ReadOnly = true;
            textBoxResultat.RightToLeft = RightToLeft.No;
            textBoxResultat.Size = new Size(800, 68);
            textBoxResultat.TabIndex = 0;
            textBoxResultat.TextAlign = HorizontalAlignment.Right;
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 4;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            tableLayoutPanel1.Controls.Add(buttonEgal, 3, 4);
            tableLayoutPanel1.Controls.Add(buttonVirgule, 2, 4);
            tableLayoutPanel1.Controls.Add(button0, 1, 4);
            tableLayoutPanel1.Controls.Add(buttonSigne, 0, 4);
            tableLayoutPanel1.Controls.Add(buttonPlus, 3, 3);
            tableLayoutPanel1.Controls.Add(button3, 2, 3);
            tableLayoutPanel1.Controls.Add(button2, 1, 3);
            tableLayoutPanel1.Controls.Add(button1, 0, 3);
            tableLayoutPanel1.Controls.Add(buttonMoins, 3, 2);
            tableLayoutPanel1.Controls.Add(button6, 2, 2);
            tableLayoutPanel1.Controls.Add(button5, 1, 2);
            tableLayoutPanel1.Controls.Add(button4, 0, 2);
            tableLayoutPanel1.Controls.Add(buttonMultiplier, 3, 1);
            tableLayoutPanel1.Controls.Add(button9, 2, 1);
            tableLayoutPanel1.Controls.Add(button8, 1, 1);
            tableLayoutPanel1.Controls.Add(button7, 0, 1);
            tableLayoutPanel1.Controls.Add(buttonDIvision, 3, 0);
            tableLayoutPanel1.Controls.Add(buttonBackspace, 2, 0);
            tableLayoutPanel1.Controls.Add(buttonC, 1, 0);
            tableLayoutPanel1.Controls.Add(buttonCE, 0, 0);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(0, 0);
            tableLayoutPanel1.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 5;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            tableLayoutPanel1.Size = new Size(800, 403);
            tableLayoutPanel1.TabIndex = 0;
            // 
            // buttonEgal
            // 
            buttonEgal.Dock = DockStyle.Fill;
            buttonEgal.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            buttonEgal.Location = new Point(603, 324);
            buttonEgal.Margin = new Padding(3, 4, 3, 4);
            buttonEgal.Name = "buttonEgal";
            buttonEgal.Size = new Size(194, 75);
            buttonEgal.TabIndex = 19;
            buttonEgal.Text = "=";
            buttonEgal.UseVisualStyleBackColor = true;
            buttonEgal.Click += buttonOperateur_Click;
            // 
            // buttonVirgule
            // 
            buttonVirgule.Dock = DockStyle.Fill;
            buttonVirgule.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            buttonVirgule.Location = new Point(403, 324);
            buttonVirgule.Margin = new Padding(3, 4, 3, 4);
            buttonVirgule.Name = "buttonVirgule";
            buttonVirgule.Size = new Size(194, 75);
            buttonVirgule.TabIndex = 18;
            buttonVirgule.Text = ",";
            buttonVirgule.UseVisualStyleBackColor = true;
            buttonVirgule.Click += buttonVirgule_Click;
            // 
            // button0
            // 
            button0.Dock = DockStyle.Fill;
            button0.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            button0.Location = new Point(203, 324);
            button0.Margin = new Padding(3, 4, 3, 4);
            button0.Name = "button0";
            button0.Size = new Size(194, 75);
            button0.TabIndex = 17;
            button0.Text = "0";
            button0.UseVisualStyleBackColor = true;
            button0.Click += buttonNum_Click;
            // 
            // buttonSigne
            // 
            buttonSigne.Dock = DockStyle.Fill;
            buttonSigne.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            buttonSigne.Location = new Point(3, 324);
            buttonSigne.Margin = new Padding(3, 4, 3, 4);
            buttonSigne.Name = "buttonSigne";
            buttonSigne.Size = new Size(194, 75);
            buttonSigne.TabIndex = 16;
            buttonSigne.Text = "+/-";
            buttonSigne.UseVisualStyleBackColor = true;
            buttonSigne.Click += buttonSigne_Click;
            // 
            // buttonPlus
            // 
            buttonPlus.Dock = DockStyle.Fill;
            buttonPlus.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            buttonPlus.Location = new Point(603, 244);
            buttonPlus.Margin = new Padding(3, 4, 3, 4);
            buttonPlus.Name = "buttonPlus";
            buttonPlus.Size = new Size(194, 72);
            buttonPlus.TabIndex = 15;
            buttonPlus.Text = "+";
            buttonPlus.UseVisualStyleBackColor = true;
            buttonPlus.Click += buttonOperateur_Click;
            // 
            // button3
            // 
            button3.Dock = DockStyle.Fill;
            button3.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            button3.Location = new Point(403, 244);
            button3.Margin = new Padding(3, 4, 3, 4);
            button3.Name = "button3";
            button3.Size = new Size(194, 72);
            button3.TabIndex = 14;
            button3.Text = "3";
            button3.UseVisualStyleBackColor = true;
            button3.Click += buttonNum_Click;
            // 
            // button2
            // 
            button2.Dock = DockStyle.Fill;
            button2.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            button2.Location = new Point(203, 244);
            button2.Margin = new Padding(3, 4, 3, 4);
            button2.Name = "button2";
            button2.Size = new Size(194, 72);
            button2.TabIndex = 13;
            button2.Text = "2";
            button2.UseVisualStyleBackColor = true;
            button2.Click += buttonNum_Click;
            // 
            // button1
            // 
            button1.Dock = DockStyle.Fill;
            button1.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            button1.Location = new Point(3, 244);
            button1.Margin = new Padding(3, 4, 3, 4);
            button1.Name = "button1";
            button1.Size = new Size(194, 72);
            button1.TabIndex = 12;
            button1.Text = "1";
            button1.UseVisualStyleBackColor = true;
            button1.Click += buttonNum_Click;
            // 
            // buttonMoins
            // 
            buttonMoins.Dock = DockStyle.Fill;
            buttonMoins.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            buttonMoins.Location = new Point(603, 164);
            buttonMoins.Margin = new Padding(3, 4, 3, 4);
            buttonMoins.Name = "buttonMoins";
            buttonMoins.Size = new Size(194, 72);
            buttonMoins.TabIndex = 11;
            buttonMoins.Text = "-";
            buttonMoins.UseVisualStyleBackColor = true;
            buttonMoins.Click += buttonOperateur_Click;
            // 
            // button6
            // 
            button6.Dock = DockStyle.Fill;
            button6.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            button6.Location = new Point(403, 164);
            button6.Margin = new Padding(3, 4, 3, 4);
            button6.Name = "button6";
            button6.Size = new Size(194, 72);
            button6.TabIndex = 10;
            button6.Text = "6";
            button6.UseVisualStyleBackColor = true;
            button6.Click += buttonNum_Click;
            // 
            // button5
            // 
            button5.Dock = DockStyle.Fill;
            button5.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            button5.Location = new Point(203, 164);
            button5.Margin = new Padding(3, 4, 3, 4);
            button5.Name = "button5";
            button5.Size = new Size(194, 72);
            button5.TabIndex = 9;
            button5.Text = "5";
            button5.UseVisualStyleBackColor = true;
            button5.Click += buttonNum_Click;
            // 
            // button4
            // 
            button4.Dock = DockStyle.Fill;
            button4.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            button4.Location = new Point(3, 164);
            button4.Margin = new Padding(3, 4, 3, 4);
            button4.Name = "button4";
            button4.Size = new Size(194, 72);
            button4.TabIndex = 8;
            button4.Text = "4";
            button4.UseVisualStyleBackColor = true;
            button4.Click += buttonNum_Click;
            // 
            // buttonMultiplier
            // 
            buttonMultiplier.Dock = DockStyle.Fill;
            buttonMultiplier.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            buttonMultiplier.Location = new Point(603, 84);
            buttonMultiplier.Margin = new Padding(3, 4, 3, 4);
            buttonMultiplier.Name = "buttonMultiplier";
            buttonMultiplier.Size = new Size(194, 72);
            buttonMultiplier.TabIndex = 7;
            buttonMultiplier.Text = "x";
            buttonMultiplier.UseVisualStyleBackColor = true;
            buttonMultiplier.Click += buttonOperateur_Click;
            // 
            // button9
            // 
            button9.Dock = DockStyle.Fill;
            button9.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            button9.Location = new Point(403, 84);
            button9.Margin = new Padding(3, 4, 3, 4);
            button9.Name = "button9";
            button9.Size = new Size(194, 72);
            button9.TabIndex = 6;
            button9.Text = "9";
            button9.UseVisualStyleBackColor = true;
            button9.Click += buttonNum_Click;
            // 
            // button8
            // 
            button8.Dock = DockStyle.Fill;
            button8.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            button8.Location = new Point(203, 84);
            button8.Margin = new Padding(3, 4, 3, 4);
            button8.Name = "button8";
            button8.Size = new Size(194, 72);
            button8.TabIndex = 5;
            button8.Text = "8";
            button8.UseVisualStyleBackColor = true;
            button8.Click += buttonNum_Click;
            // 
            // button7
            // 
            button7.Dock = DockStyle.Fill;
            button7.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            button7.Location = new Point(3, 84);
            button7.Margin = new Padding(3, 4, 3, 4);
            button7.Name = "button7";
            button7.Size = new Size(194, 72);
            button7.TabIndex = 4;
            button7.Text = "7";
            button7.UseVisualStyleBackColor = true;
            button7.Click += buttonNum_Click;
            // 
            // buttonDIvision
            // 
            buttonDIvision.Dock = DockStyle.Fill;
            buttonDIvision.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            buttonDIvision.Location = new Point(603, 4);
            buttonDIvision.Margin = new Padding(3, 4, 3, 4);
            buttonDIvision.Name = "buttonDIvision";
            buttonDIvision.Size = new Size(194, 72);
            buttonDIvision.TabIndex = 3;
            buttonDIvision.Text = "/";
            buttonDIvision.UseVisualStyleBackColor = true;
            buttonDIvision.Click += buttonOperateur_Click;
            // 
            // buttonBackspace
            // 
            buttonBackspace.Dock = DockStyle.Fill;
            buttonBackspace.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            buttonBackspace.Location = new Point(403, 4);
            buttonBackspace.Margin = new Padding(3, 4, 3, 4);
            buttonBackspace.Name = "buttonBackspace";
            buttonBackspace.Size = new Size(194, 72);
            buttonBackspace.TabIndex = 2;
            buttonBackspace.Text = "<=";
            buttonBackspace.UseVisualStyleBackColor = true;
            buttonBackspace.Click += buttonBackspace_Click;
            // 
            // buttonC
            // 
            buttonC.Dock = DockStyle.Fill;
            buttonC.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            buttonC.Location = new Point(203, 4);
            buttonC.Margin = new Padding(3, 4, 3, 4);
            buttonC.Name = "buttonC";
            buttonC.Size = new Size(194, 72);
            buttonC.TabIndex = 1;
            buttonC.Text = "C";
            buttonC.UseVisualStyleBackColor = true;
            buttonC.Click += buttonC_Click;
            // 
            // buttonCE
            // 
            buttonCE.Dock = DockStyle.Fill;
            buttonCE.Font = new Font("Microsoft Sans Serif", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 0);
            buttonCE.Location = new Point(3, 4);
            buttonCE.Margin = new Padding(3, 4, 3, 4);
            buttonCE.Name = "buttonCE";
            buttonCE.Size = new Size(194, 72);
            buttonCE.TabIndex = 0;
            buttonCE.Text = "CE";
            buttonCE.UseVisualStyleBackColor = true;
            buttonCE.Click += buttonCE_Click;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 562);
            Controls.Add(splitContainer1);
            Margin = new Padding(3, 4, 3, 4);
            Name = "Form1";
            Text = "Calculatrice V2";
            splitContainer1.Panel1.ResumeLayout(false);
            splitContainer1.Panel1.PerformLayout();
            splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitContainer1).EndInit();
            splitContainer1.ResumeLayout(false);
            tableLayoutPanel1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox textBoxResultat;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button buttonEgal;
        private System.Windows.Forms.Button buttonVirgule;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button buttonSigne;
        private System.Windows.Forms.Button buttonPlus;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonMoins;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button buttonMultiplier;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button buttonDIvision;
        private System.Windows.Forms.Button buttonBackspace;
        private System.Windows.Forms.Button buttonC;
        private System.Windows.Forms.Button buttonCE;
    }
}

