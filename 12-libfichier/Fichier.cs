﻿using _12_libfichier;

namespace _12_Libfichier
{
    public static class Fichier
    {
        public static void InfoLecteur()
        {
            // DriveInfo fournit des informations sur les lecteurs d'une machine
            DriveInfo[] lecteurs = DriveInfo.GetDrives();
            foreach (var drv in lecteurs)
            {
                if (drv.IsReady)
                {
                    Console.WriteLine(drv.Name);
                    Console.WriteLine(drv.TotalSize);       // si lelecteur n'est pas prêt -> exception 
                    Console.WriteLine(drv.TotalFreeSpace);  // ...
                    Console.WriteLine(drv.DriveType);       // ...
                    Console.WriteLine(drv.DriveFormat);     // Idem
                }
                else
                {
                    Console.WriteLine($"{drv.Name} n'est pas pret ");
                }

            }
        }

        public static void InfoDossier()
        {
            //  Teste si le dossier existe
            if (Directory.Exists(@"C:\Dawan\Formation"))
            {
                // Liste les répertoires contenu dans le chemin
                string[] rep = Directory.GetDirectories(@"C:\Dawan\Formation");
                foreach (string r in rep)
                {
                    Console.WriteLine(r);
                }

                // Liste les fichiers du répertoire
                string[] fi = Directory.GetFiles(@"C:\Dawan\Formation");
                foreach (string f in fi)
                {
                    Console.WriteLine(f);
                }
                if (Directory.Exists(@"C:\Dawan\Formation\vide"))
                {
                    Directory.Delete(@"C:\Dawan\Formation\vide");
                }
            }
            else
            {
                // Création du répertoire
                Directory.CreateDirectory(@"C:\Dawan\Formation");
            }
        }


        public static void Parcourir(string path)
        {
            if (Directory.Exists(path))
            {
                string[] file = Directory.GetFiles(path);
                foreach (var f in file)
                {
                    Console.WriteLine(f);
                }
                string[] rep = Directory.GetDirectories(path);
                foreach (var r in rep)
                {
                    Console.WriteLine($"Repertoire {r}");
                    Console.WriteLine("-----------------------------------------------");
                    Parcourir(r);
                }
            }
        }

        public static void InfoFichier(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        public static void ArticlesToCsv(List<Article> articles, string path)
        {
            StreamWriter sw = null; // StreamWriter Ecrire un fichier texte
            try
            {
                sw = new StreamWriter(path, true);    // append à true => compléte le fichier s'il existe déjà, à false le fichier est écrasé
                foreach (var a in articles)
                {
                    string ligne = string.Join(";", a.Nom, a.Description, a.Prix);
                    sw.WriteLine(ligne);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                sw?.Close();
                sw?.Dispose();
            }
        }

        // Using => Équivalent d'un try / finally + Close()
        public static List<Article> ArticleFromCsv(string path)
        {
            List<Article> articles = new List<Article>();
            using (StreamReader sr = new StreamReader(path))    // StreamReader lire un fichier texte
            {
                while (!sr.EndOfStream)
                {
                    string ligne = sr.ReadLine();
                    string[] element = ligne.Split(";");
                    Article article = new Article(element[1], Convert.ToDouble(element[2]), element[0]);
                    articles.Add(article);
                }
            }
            return articles;
        }
        public static void EcrireFichierBinaire(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Create))   // FileStream =>  permet de Lire/Ecrire un fichier binaire
            {
                for (byte b = 0; b < 104; b++)
                {
                    fs.WriteByte(b);    // Ecriture d'un octet dans le fichier
                }
            }
        }

        public static void LireFichierBinaire(string path)
        {
            byte[] buf = new byte[10];
            int nb = 1;

            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                while (nb != 0)
                {
                    nb = fs.Read(buf, 0, buf.Length);   // Lecture de 10 octets au maximum dans le fichier, ils sont placés dans le tableau tab à partir de l'indice 0
                    for (int i = 0; i < nb; i++)        // Read => retourne le nombre d'octets lue dans le fichier
                    {
                        Console.Write($"{buf[i]}");
                    }
                    Console.WriteLine();
                }
            }
        }


        // Copie d'un fichier binaire
        public static void Copie(string source, string cible)
        {
            // FileStream =>  permet de Lire/Ecrire un fichier binaire
            byte[] t = new byte[1000];
            FileStream fi = null;
            FileStream fo = null;
            try
            {
                fi = new FileStream(source, FileMode.Open);
                fo = new FileStream(cible, FileMode.CreateNew);
                while (true)
                {
                    // Lit 1000 octets au maximum dans le fichier (source) et les place dans le tableau à partir de l'indice 0
                    // Read => retourne le nombre d'octets lue dans le fichier
                    int size = fi.Read(t, 0, t.Length);
                    if (size == 0)
                    {
                        break;
                    }
                    // écrit dans le fichier (cible) les octets qui ont été lue
                    fo.Write(t, 0, size);
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                fi?.Close();
                fo?.Close();
                fi?.Dispose();
                fo?.Dispose();
            }
        }
    }
}
