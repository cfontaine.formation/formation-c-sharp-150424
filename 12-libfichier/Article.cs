﻿namespace _12_Libfichier
{
    public class Article
    {
        public string Description { get; set; }

        public double Prix { get; set; }

        public string Nom { get; set; }

        public Article(string description, double prix, string nom)
        {
            Description = description;
            Prix = prix;
            Nom = nom;
        }

        public override string? ToString()
        {
            return $"{Description} {Prix} {Nom}";
        }
    }
}
