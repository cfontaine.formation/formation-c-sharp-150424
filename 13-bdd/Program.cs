﻿using _13_libBddMySql;

//using _13_libBddSqlServer; // changer la référence de projet vers le projet sqlserver
using MySql.Data.MySqlClient;


namespace _13_utilisationBdd
{
    internal class Program
    {
        static void Main()
        {
            Contact contact = SaisirContact();
            // TestBddMySQL(contact);
            // TestBddSQLServer(contact);
            TestDao(contact, "Server=localhost;Port=3306;Database=formation_c;Uid=root;Pwd=;SslMode=none");
        }

        static void TestBddMySQL(Contact contact)
        {
            // La chaine de connection contient toutes les informations pour la connection à la base de donnée
            // Server-> adresse du serveur de bdd
            // Port port de serveur de bdd
            // Database -> nom de la base de donnée
            // Uid -> utilisateur de la base de donnée
            // Pwd -> mot de passe de la bdd

            //  Chaine de connection MySQL en local
            string chaineConnexion = "Server=localhost;Port=3306;Database=formation_c;Uid=root;Pwd=;SslMode=none";

            // Création de la connexion à la base de donnée MySqlConnection
            MySqlConnection cnx = new MySqlConnection(chaineConnexion);

            // Ouverture de la connexion à la base de donnée
            cnx.Open();
            string requete = "INSERT INTO contacts(prenom,nom,email,date_naissance) VALUES(@prenom,@nom,@email,@dateNaissance)";

            MySqlCommand cmd = new MySqlCommand(requete, cnx);
            // Remplacement des paramètres dans la requête (@...) par les valeurs
            cmd.Parameters.AddWithValue("@prenom", contact.Prenom);
            cmd.Parameters.AddWithValue("@nom", contact.Nom);
            cmd.Parameters.AddWithValue("@email", contact.Email);
            cmd.Parameters.AddWithValue("@dateNaissance", contact.DateNaissance);
            cmd.ExecuteNonQuery();

            requete = "SELECT id,prenom,nom,email,jour_naissance FROM Contacts";
            cmd = new MySqlCommand(requete, cnx);
            MySqlDataReader r = cmd.ExecuteReader();
            while (r.Read())    // Read() permet de passer à la prochaine "ligne" retourne false quand il n'y a plus de resultat
            {
                Console.WriteLine($"{r.GetInt64(0)} {r.GetString(1)}  {r.GetString(2)}  {r.GetString(3)} {r.GetDateTime(4).ToShortDateString()} ");
            }
            // Fermeture de la connection
            cnx.Close();
            cnx.Dispose();
        }

        /*    static void TestBddSqlServer(Contact contact)
            {
                // Chaine de connection SQLServer sur un serveur
                string chaineConnection = "Server=192.168.1.9;Database=formation_ch;Uid=sa;Pwd=Dawan_2022!sql";

                // Chaine de connection pour se connecter à Sqlserver express en local avec une authentification avec l'utilisateur windows
                // string chaineConnection = @"Data Source=DESKTOP-9H6VFME\SQLEXPRESS;Initial Catalog=formation_ch;Integrated Security= True";

                // Création de la connexion à la base de donnée SqlConnection
                SqlConnection cnx = new SqlConnection(chaineConnection);

                // Ouverture de la connexion à la base de donnée
                cnx.Open();

                string requete = "INSERT INTO contacts(prenom,nom,email,date_naissance) VALUES (@prenom,@nom,@email,@datenaissance)";
                SqlCommand cmd = new SqlCommand(requete, cnx);
                // Remplacement des paramètres dans la requête (@...) par les valeurs
                cmd.Parameters.AddWithValue("@prenom", "john");
                cmd.Parameters.AddWithValue("@nom", "Doe");
                cmd.Parameters.AddWithValue("@email", "jd@dawan.com");
                cmd.Parameters.AddWithValue("@datenaissance", new DateTime(1998, 03, 01));
                cmd.ExecuteNonQuery();  // execution de la requete pour INSERT,UPDATE,DELETE

                requete = "SELECT id,prenom,nom,email,date_naissance FROM contacts";
                cmd = new SqlCommand(requete, cnx);
                SqlDataReader r = cmd.ExecuteReader();
                while (r.Read())    // Read() permet de passer à la prochaine "ligne" retourne false quand il n'y a plus de resultat
                {
                    Console.WriteLine($"{r.GetInt64(0)} {r.GetString(1)}  {r.GetString(2)}  {r.GetString(3)} {r.GetDateTime(4).ToShortDateString()} ");
                }
                // Fermeture de la connection
                cnx.Close();
                cnx.Dispose();
            }
        */

        static void TestDao(Contact c,string chaineConnection)
        {
            ContactDao dao = new ContactDao();
            ContactDao.ChaineConnection = chaineConnection;
            Console.WriteLine("id={0}", c.Id); // id=0 => l'objet n'a pas été peristé dans la base de donnée
            dao.SaveOrUpdate(c,false); // persister l'objet dans la bdd
            Console.WriteLine("id={0}", c.Id); // id a été généré par la bdd
            Console.WriteLine("{0}", c);

            long id = c.Id;
            // Affichage de tous les objets contact
            List<Contact> lst = dao.FindAll(false);
            foreach (Contact co in lst)
            {
                Console.WriteLine(co);
            }

            // Lire un objet à partir de son id
            Console.WriteLine("\nLire {0}", id);
            Contact cr = dao.FindByid(id, false);
            Console.WriteLine(cr);

            // Modification 
            Console.WriteLine("\nModification {0}", cr.Id);
            cr.Prenom = "Marcel";
            cr.DateNaissance = new DateTime(1987, 8, 11);
            dao.SaveOrUpdate(cr, false);
            cr = dao.FindByid(cr.Id, false);
            Console.WriteLine(cr);

            // Effacer
            Console.WriteLine("\neffacer {0}", cr.Id);
            dao.Remove(cr.Id, false);
            lst = dao.FindAll();
            foreach (Contact co in lst)
            {
                Console.WriteLine(co);
            }
        }

        private static Contact SaisirContact()
        {
            Console.Write("Entrer votre prénom: ");
            string prenom = Console.ReadLine();
            Console.Write("Entrer votre nom: ");
            string nom = Console.ReadLine();
            Console.Write("Entrer votre email: ");
            string email = Console.ReadLine();
            Console.Write("Entrer votre date de naissance (YYYY/MM/DD): ");
            DateTime dn = DateTime.Parse(Console.ReadLine());
            return new Contact(prenom, nom, email, dn);
        }

    }
}
