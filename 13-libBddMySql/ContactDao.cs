﻿using MySql.Data.MySqlClient;

namespace _13_libBddMySql
{
    public class ContactDao
    {
        private static MySqlConnection cnx;
        public static string ChaineConnection { get; set; }

        public void SaveOrUpdate(Contact contact, bool close = true)
        {
            if ((contact.Id == 0))
            {
                Create(contact, close);
            }
            else
            {
                Update(contact, close);
            }
        }

        public void Remove(long id, bool close = true)
        {
            string req = "DELETE FROM contacts WHERE id=@id";
            using (MySqlCommand cmd = new MySqlCommand(req, GetConnection()))
            {
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
            }
            CloseConnection(close);
        }

        public List<Contact> FindAll(bool close = true)
        {
            List<Contact> list = new List<Contact>();
            string requete = "SELECT id,prenom,nom,email,date_naissance FROM contacts";
            MySqlCommand cmd = new MySqlCommand(requete, GetConnection());
            using (MySqlDataReader r = cmd.ExecuteReader())
            {
                while (r.Read())
                {
                    Contact c = new Contact(r.GetString("prenom"), r.GetString("nom"), r.GetString("email"), r.GetDateTime("date_naissance"));
                    c.Id = r.GetInt64("id");
                    list.Add(c);
                }
            }
            CloseConnection(close);
            return list;
        }

        public Contact FindByid(long id, bool close = true)
        {
            Contact contact = null;
            string req = "SELECT prenom,nom,email,date_naissance FROM contacts  WHERE id=@id";
            using (MySqlCommand cmd = new MySqlCommand(req, cnx))
            {
                cmd.Parameters.AddWithValue("@id", id);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        contact = new Contact(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetDateTime(3));
                        contact.Id = id;
                    }
                }
            }
            return contact;

        }

        private void Create(Contact contact, bool close = true)
        {
            string requete = "INSERT INTO contacts(prenom,nom,email,date_naissance) VALUES(@prenom,@nom,@email,@dateNaissance)";
            MySqlCommand cmd = new MySqlCommand(requete, GetConnection());
            cmd.Parameters.AddWithValue("@prenom", contact.Prenom);
            cmd.Parameters.AddWithValue("@nom", contact.Nom);
            cmd.Parameters.AddWithValue("@email", contact.Email);
            cmd.Parameters.AddWithValue("@dateNaissance", contact.DateNaissance);
            cmd.ExecuteNonQuery();
            contact.Id = cmd.LastInsertedId;
            CloseConnection(close);
        }

        private void Update(Contact contact, bool close = true)
        {
            string req = "UPDATE contacts SET nom = @nom, prenom=@prenom, email=@email, date_naissance=@date_naissance WHERE id=@id";
            using (MySqlCommand cmd = new MySqlCommand(req, GetConnection()))
            {
                cmd.Parameters.AddWithValue("@id", contact.Id);
                cmd.Parameters.AddWithValue("@prenom", contact.Prenom);
                cmd.Parameters.AddWithValue("@nom", contact.Nom);
                cmd.Parameters.AddWithValue("@date_naissance", contact.DateNaissance);
                cmd.Parameters.AddWithValue("@email", contact.Email);
                cmd.ExecuteNonQuery();
                CloseConnection(close);
            }
        }

        private MySqlConnection GetConnection()
        {
            if (cnx == null)
            {
                cnx = new MySqlConnection(ChaineConnection);
                cnx.Open();
            }
            return cnx;
        }

        private void CloseConnection(bool close)
        {
            if (close && cnx != null)
            {
                cnx.Close();
                cnx.Dispose();
                cnx = null;
            }
        }

        // Dans le dao, on peut déclarer d'autre méthodes suplémentaires uniquement pour la classe Contact 
        public bool IsEmailExist(string email, bool close = true)   // test si un email existe dans la base de donnée
        {
            bool exist = false;
            string req = "SELECT email FROM contacts WHERE email=@email";
            using (MySqlCommand cmd = new MySqlCommand(req, GetConnection()))
            {
                cmd.Parameters.AddWithValue("@email", email);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        exist = true;
                    }
                }
            }
            CloseConnection(close);
            return exist;
        }

        public Contact FindByEmail(string email, bool close = true)    // Trouve un contact dans la bdd en fonction d'un email 
        {
            Contact contact = null;
            string req = "SELECT id,prenom,nom,date_naissance FROM contacts WHERE email=@email";
            using (MySqlCommand cmd = new MySqlCommand(req, GetConnection()))
            {
                cmd.Parameters.AddWithValue("@email", email);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        contact = new Contact(reader.GetString(1), reader.GetString(2), email, reader.GetDateTime(3));
                        contact.Id = reader.GetInt64(0);
                    }
                }
            }
            CloseConnection(close);
            return contact;
        }




    }
}
