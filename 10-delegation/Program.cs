﻿using System.Security.Cryptography.X509Certificates;

namespace _10_delegation
{
    internal class Program
    {
        // prototype de méthode
        public delegate int Operation(int a, int b);

        public delegate bool Comparaison(int a, int b);

        // C# 1.0
        // ---------------------------------------
        public static int Ajouter(int n1, int n2)
        {
            return n1 + n2;
        }

        public static int Multiplier(int n1, int n2)
        {
            return n1 * n2;
        }
        // ---------------------------------------
        static void Main(string[] args)
        {
            // C# 1.0
            Operation add = new Operation(Ajouter);
            Console.WriteLine(Calculer(1, 3, add));
            Console.WriteLine(Calculer(2, 3, new Operation(Multiplier)));
            // C# 2.0
            Operation add2 = delegate (int i, int j) { return i + j; };
            Console.WriteLine(Calculer(1, 3, add2));
            Console.WriteLine(Calculer(1, 3, delegate (int i1, int i2) { return i1 * i2; }));
            // C# 3.0 expression lambda
            Console.WriteLine(Calculer(1, 3, (int i, int j) => { return i + j; })); // à simplifier
            Console.WriteLine(Calculer(1, 3, (i, j) => i + j));

            Console.WriteLine(Calculer(1, 3, (i, j) => i * j));
            Console.WriteLine(Calculer(1, 3, (i, j) => i - j));
            Console.WriteLine(Calculer(1, 3, (i, j) =>
            {
                int res = i - j;   // si l'expression lambda à plusieurs lignes
                return res;
            }));

            int[] tab = { 10, 4, 6, 12, -1, 9 };
            SortTab(tab, (a, b) => a < b);
            foreach (var e in tab)
            {
                Console.WriteLine(e);
            }

            // Délégué Func et Action
            Func<double, double, double> multi = (x, y) => x * y;
            Console.WriteLine(Calculer(2.3, 1.4, multi));
            Console.WriteLine(Calculer(2.3, 1.4, (a, b) => a - b));

            // événement
            CompteBancaire cb = new CompteBancaire(300.0);
            cb.OnSoldeNegatif += SmsSoldeNegatif;
            cb.OnSoldeNegatif += EmailSoldeNegatif;
            cb.Debiter(100.0);
            Console.WriteLine(cb);
            Console.WriteLine("------------------------");
            cb.Debiter(400.0);
        }

        public static int Calculer(int v1, int v2, Operation op)
        {
            return op(v1, v2);
        }

        public static double Calculer(double v1, double v2, Func<double, double, double> op)
        {
            return op(v1, v2);
        }

        static void SortTab(int[] tab, Func<int, int, bool> cmp)
        // static void SortTab(int[] tab, Comparaison cmp)
        {
            bool notDone = true;
            int end = tab.Length - 1;
            while (notDone)
            {
                notDone = false;
                for (int i = 0; i < end; i++)
                {
                    if (cmp(tab[i], tab[i + 1]))
                    {
                        int tmp = tab[i];
                        tab[i] = tab[i + 1];
                        tab[i + 1] = tmp;
                        notDone = true;
                    }
                }
                end--;
            }
        }

        static void SmsSoldeNegatif(object sender, SoldeNegatifArgs args)
        {
            Console.WriteLine($"SMS  solde négatif={args.Solde}");
        }

        static void EmailSoldeNegatif(object sender, SoldeNegatifArgs args)
        {
            Console.WriteLine($" Email solde négatif={args.Solde}");
        }

    }
}
