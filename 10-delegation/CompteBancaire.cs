﻿namespace _10_delegation
{
    internal class CompteBancaire
    {
        // La classe qui contient l’évènement doit d’abord déclarer un délégué
        // public delegate void BalanceDelegate();

        // l’évènement sera du même type que ce délégué
        // Par convention, le nom d’un évènement est de la forme On<Nom de l’évènement>

        // Déclaration de l'évènement
        // public event BalanceDelegate OnSoldeNegatif;

        // Ou, on peut déclarer l'événement en utilisant le délégué EventHandler fournit par le Framework.NET -> EventHandler<TEvenArgs>
        public event EventHandler<SoldeNegatifArgs> OnSoldeNegatif;
        public double Solde { get; set; }

        public CompteBancaire(double solde)
        {
            Solde = solde;
        }

        public void Crediter(int valeur)
        {
            Solde += valeur;
        }

        public void Debiter(double valeur)
        {
            Solde -= valeur;
            if (Solde < 0)
            {
                OnSoldeNegatif(this, new SoldeNegatifArgs(Solde));   // S'il n'y a pas de paramètre => EventArgs.Empty
            }
        }

        public override string? ToString()
        {
            return $"Solde={Solde}";
        }
    }
}
