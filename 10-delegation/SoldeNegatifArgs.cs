﻿namespace _10_delegation
{
    internal class SoldeNegatifArgs : EventArgs
    {
        public double Solde { get; set; }

        public SoldeNegatifArgs(double solde)
        {
            Solde = solde;
        }
    }
}
