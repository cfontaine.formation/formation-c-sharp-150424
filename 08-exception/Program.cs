﻿namespace _08_exception
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int[] tb = new int[4];
                int index = Convert.ToInt32(Console.ReadLine());    // Peut générer une exception FormatException, si on saisie autre chose qu'un nombre entier
                Console.WriteLine(tb[index]);                       // Peut générer une exception IndexOutOfRangeException , si index est > à Length
                int age = Convert.ToInt32(Console.ReadLine());       // Peut générer une exception FormatException, si on saisie autre chose qu'un nombre entier

                TraitementEmploye(age);
                Console.WriteLine("Suite programme");
            }
            catch (IndexOutOfRangeException e)  // Attrape les exceptions IndexOutOfRangeException
            {
                Console.WriteLine("En dehors des limites du tableau");
                Console.WriteLine(e.Message);   // Message => permet de récupérer le messsage de l'exception
            }
            catch (FormatException e)   // Attrape les exceptions FormatException
            {
                Console.WriteLine("Erreur de format");
                Console.WriteLine(e.StackTrace);
            }
            catch (Exception e)     // Attrape tous les autres exception
            {
                Console.WriteLine("Autre Exception");
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.InnerException?.StackTrace);
            }
            finally
            // Le bloc finally est toujours éxécuté
            {
                // finally -> généralement utilisé pour libérer les ressources
                Console.WriteLine("Toujours executé");
            }
            // System.Environment.Exit(0);
            Console.WriteLine("fin programme");
        }

        public static void TraitementEmploye(int age)
        {
            Console.WriteLine("début traitement employé");
            // Traitement partiel de l'exception AgeNegatifException
            try
            {
                TraitementAge(age);
            }
            catch (AgeNegatifException e) when (e.Age == 0)
            {
                // Traitement partiel de l'exception AgeNegatifException
                Console.WriteLine("traitement local ==0:" + e.Message + " " + e.Age);
                // relance d'exception
                // On relance l'exception pour que l'utilisateur de la méthode la traite à son niveau
                //  throw; 

                // On relance une exception différente, e => référence à l'exception qui a provoquer l'exception
                throw new ArgumentException("Traiement employé age négatif", e);
            }
            catch (AgeNegatifException e) when (e.Age < 0)
            {
                Console.WriteLine("traitement local <0:" + e.Message + " " + e.Age);
            }
            Console.WriteLine("fin traitement employé");
        }

        public static void TraitementAge(int age)
        {
            Console.WriteLine("début traitement age");
            if (age < 0)
            {
                throw new AgeNegatifException(age, "age négatif");   // throw => Lancer un exception
                                                                     //  Elle va remonter la pile d'appel des méthodes jusqu'à ce qu'elle soit traité par un try/catch
                                                                     //  si elle n'est pas traiter, aprés la méthode Main => arrét du programme

            }
            Console.WriteLine("fin traitement age");
        }
    }
}
