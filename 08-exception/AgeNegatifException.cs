﻿namespace _08_exception
{
    // Exception personnalisée => on hérite de la classe Exception ou d'une classe enfant de Exceptio
    internal class AgeNegatifException : Exception
    {
        public int Age { get; set; }
        public AgeNegatifException(int age)
        {
            Age = age;
        }

        public AgeNegatifException(int age, string? message) : base(message)
        {
            Age = age;
        }

        public AgeNegatifException(int age, string? message, Exception? innerException) : base(message, innerException)
        {
            Age = age;
        }
    }
}
