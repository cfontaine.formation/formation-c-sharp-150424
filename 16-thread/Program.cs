﻿using System.Diagnostics;

namespace _16_thread
{
    internal class Program
    {

        static void Main()
        {

            Thread.CurrentThread.Name = "Main";

            Console.WriteLine("Début de la méthode Main");

            #region Classe Thread
            // ThreadStart -> Délégué qui permet de créer Thread (Méthode sans paramètre)
            Thread tsp = new Thread(new ThreadStart(ThreadSansParam)); // Le constructeur de la classe Thread prend en paramètre un délégué
            tsp.Start(); //  La Méthode Start permet de démmarrer le Thread

            // ParamterizedThreadStart -> Délégué qui permet de créer Thread (méthode qui a un paramètre de "type" object)
            Thread tap = new Thread(new ParameterizedThreadStart(ThreadMethode));
            tap.Start(50);
            #endregion

            #region Propriété
            Thread t1 = new Thread(new ParameterizedThreadStart(ThreadMethode));
            t1.Name = "T1";    // La propriété Name permet de nommer le Thread
                               // t1.Priority = ThreadPriority.Lowest;  // La propriété Priority permet de modifier la prioritédu thread
                               // Highest => le thread est le + prioritaire, AboveNormal, Normal (par défaut), BelowNormal, Lowest => le thread est le - prioritaire
            Console.WriteLine($"état du Thread à la création = {t1.ThreadState}");    // La propriétée ThreadState contient l'état du Thread, au départ => Unstarted
            t1.Start(50);
            Console.WriteLine($"état du Thread après L'éxécution de la méthode Start= {t1.ThreadState}"); // Runnable ou running s'il est executé
            #endregion

            // Thread Main
            for (int i = 0; i < 40; i++)
            {
                Console.WriteLine($"Main - {i}");
                Thread.Sleep(50);
            }

            #region Thread join
            // Join 
            // bloque tous les threads appelants jusqu'à ce que ce thread se termine
            Thread t2 = new Thread(new ParameterizedThreadStart(ThreadMethode));
            t2.Start();
            t2.Join(100);  // Attend que le thread t2 soit finit pour continuer

            // Thread Main
            for (int i = 0; i < 30; i++)
            {
                Console.WriteLine($"Main - {i}");
                Thread.Sleep(50);
            }
            #endregion

            #region Timer
            Timer t = new Timer(TestTimer, "Message du timer",
            0,          //appel tout de suite
            1000);      //et toutes les secondes
            #endregion

            #region Processus
            Process p2 = Process.Start("Notepad.exe");
            #endregion
            Console.WriteLine("Fin de la méthode Main");
            Console.ReadKey();
        }
        static void ThreadSansParam()
        {
            for (int i = 0; i < 30; i++)
            {
                Console.WriteLine($"Thread sans paramètre i={i}");
            }
        }

        static void ThreadMethode(object time)
        {
            Thread current = Thread.CurrentThread;  // propriété statique qui permet d'accéder au thread en cours
            Console.WriteLine($"Début du Thread {current.Name} {current.Priority} {time}ms ");
            for (int i = 0; i < 30; i++)
            {
                Console.WriteLine($"Thread {current.Name} i={i}");
                if (time is int t)
                {
                    Thread.Sleep(t);   // Pour suspendre le thread actuel pendant les t millisecondes
                }
            }
            Console.WriteLine($"Fin du Thread {current.Name}");

        }

        static void TestTimer(object state)
        {
            Console.WriteLine(state);
        }
    }
}
