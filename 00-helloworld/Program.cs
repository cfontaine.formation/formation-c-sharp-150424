﻿/// <summary>
/// La classe Program
/// </summary>
internal class Program
{
    /// <summary>
    /// Le point d'entrée du programme
    /// </summary>
    /// <param name="args">Les paramètres de ligne de commande</param>
    private static void Main(string[] args) // Commentaire fin de ligne
    {
        /* Commentaire
         * sur 
         * plusieurs lignes*/

        Console.WriteLine("Hello, World!");
    }
}