
namespace _17_Annuaire_winform
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.prenomTextbox = new System.Windows.Forms.TextBox();
            this.nomTextbox = new System.Windows.Forms.TextBox();
            this.emailTextbox = new System.Windows.Forms.TextBox();
            this.dateNaissancePicker = new System.Windows.Forms.DateTimePicker();
            this.addModifBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Prénom";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nom";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 220);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Email";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 166);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Date naissance";
            // 
            // prenomTextbox
            // 
            this.prenomTextbox.Location = new System.Drawing.Point(198, 49);
            this.prenomTextbox.Name = "prenomTextbox";
            this.prenomTextbox.Size = new System.Drawing.Size(290, 22);
            this.prenomTextbox.TabIndex = 4;
            // 
            // nomTextbox
            // 
            this.nomTextbox.Location = new System.Drawing.Point(198, 105);
            this.nomTextbox.Name = "nomTextbox";
            this.nomTextbox.Size = new System.Drawing.Size(290, 22);
            this.nomTextbox.TabIndex = 5;
            // 
            // emailTextbox
            // 
            this.emailTextbox.Location = new System.Drawing.Point(198, 217);
            this.emailTextbox.Name = "emailTextbox";
            this.emailTextbox.Size = new System.Drawing.Size(290, 22);
            this.emailTextbox.TabIndex = 6;
            // 
            // dateNaissancePicker
            // 
            this.dateNaissancePicker.Location = new System.Drawing.Point(198, 161);
            this.dateNaissancePicker.Name = "dateNaissancePicker";
            this.dateNaissancePicker.Size = new System.Drawing.Size(290, 22);
            this.dateNaissancePicker.TabIndex = 7;
            // 
            // addModifBtn
            // 
            this.addModifBtn.Location = new System.Drawing.Point(61, 297);
            this.addModifBtn.Name = "addModifBtn";
            this.addModifBtn.Size = new System.Drawing.Size(162, 53);
            this.addModifBtn.TabIndex = 8;
            this.addModifBtn.Text = "Ajouter";
            this.addModifBtn.UseVisualStyleBackColor = true;
            this.addModifBtn.Click += new System.EventHandler(this.addModifBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelBtn.Location = new System.Drawing.Point(299, 297);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(162, 53);
            this.cancelBtn.TabIndex = 9;
            this.cancelBtn.Text = "Annuler";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // Form2
            // 
            this.AcceptButton = this.addModifBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelBtn;
            this.ClientSize = new System.Drawing.Size(528, 379);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.addModifBtn);
            this.Controls.Add(this.dateNaissancePicker);
            this.Controls.Add(this.emailTextbox);
            this.Controls.Add(this.nomTextbox);
            this.Controls.Add(this.prenomTextbox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form2";
            this.Text = "Ajouter un contact";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox prenomTextbox;
        private System.Windows.Forms.TextBox nomTextbox;
        private System.Windows.Forms.TextBox emailTextbox;
        private System.Windows.Forms.DateTimePicker dateNaissancePicker;
        private System.Windows.Forms.Button addModifBtn;
        private System.Windows.Forms.Button cancelBtn;
    }
}