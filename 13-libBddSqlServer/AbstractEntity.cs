﻿namespace _13_libBddSqlServer
{
    // La classe abstract AbstractEntity est la classe mère des objets persistés dans la base de données
    // Elle gère l'id qui fait le lien entre l'objet et la table des la bdd (clé primaire)
    public abstract class AbstractEntity
    {
        public long Id { get; set; }

        public override string ToString()
        {
            return $"{Id}";
        }
    }
}
