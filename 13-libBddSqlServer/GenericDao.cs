﻿using System.Data.SqlClient;

namespace _13_libBddSqlServer
{
    // La classe abstraite GenericDao est la classe mère de tous les dao
    // Elle permet de gérer la connection à la base de données et fournit les méthodes Create Read Update Delete 
    public abstract class GenericDao<T> where T : AbstractEntity
    {
        public static string ChaineConnection { get; set; }

        // AVEC NUGET, il faut installer la dépendance : System.Data.SqlClient
        public static SqlConnection cnx;

        // Méthode pour persiter un objet ou le mettre à jour s'il existe dans la base de donnée
        // close à false permet de ne pas fermer la connection à la base de donnée
        public void SaveOrUpdate(T entity, bool close = true)
        {
            if (entity.Id == 0)
            {
                Create(GetConnection(), entity);
            }
            else
            {
                Update(GetConnection(), entity);
            }
            CloseConnection(close);
        }

        // Méthode pour supprimer un objet de la base de données
        public void Delete(T entity, bool close = true)
        {
            Delete(GetConnection(), entity.Id);
            CloseConnection(close);
        }

        public void Delete(long id, bool close = true)
        {
            Delete(GetConnection(), id);
            CloseConnection(close);
        }

        // Méthode pour récupérer un objet dans la base de donnée en fonction de son id
        public T FindById(long id, bool close = true)
        {
            T e = FindById(GetConnection(), id);
            CloseConnection(close);
            return e;
        }

        // Méthode pour récupérer tous les objets de la base de donnée
        public List<T> FindAll(bool close = true)
        {
            List<T> e = FindAll(GetConnection());
            CloseConnection(close);
            return e;
        }

        // Méthode qui permet d'obtenir la connection à la base de donnée
        protected SqlConnection GetConnection()
        {
            if (cnx == null)
            {
                cnx = new SqlConnection(ChaineConnection);
                cnx.Open();
            }
            return cnx;
        }

        // Méthode qui permet de fermer la connection à la base de donnée
        protected void CloseConnection(bool close)
        {
            if (cnx != null && close)
            {
                cnx.Close();
                cnx.Dispose();
                cnx = null;
            }
        }

        // Méthodes abstraites qui vont contenir les requêtes SQL dans les sous-classes
        protected abstract void Create(SqlConnection cnx, T elm);
        protected abstract void Update(SqlConnection cnx, T elm);
        protected abstract void Delete(SqlConnection cnx, long id);
        protected abstract T FindById(SqlConnection cnx, long id);
        protected abstract List<T> FindAll(SqlConnection cnx);

    }

}
