﻿namespace _13_libBddSqlServer
{
    public class Contact : AbstractEntity
    {
        public string Prenom { get; set; }
        public string Nom { get; set; }

        public string Email { get; set; }

        public DateTime DateNaissance { get; set; }

        public Contact(string prenom, string nom, string email, DateTime dateNaissance)
        {
            Prenom = prenom;
            Nom = nom;
            Email = email;
            DateNaissance = dateNaissance;
        }

        public override string ToString()
        {
            return $"{base.ToString()} {Prenom} {Nom} {Email} {DateNaissance}";
        }
    }

}
