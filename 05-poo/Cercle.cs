﻿namespace _05_poo
{
    internal class Cercle
    {
        public Point Centre { get; set; }

        public double Rayon { get; set; } = 1.0;


        public Cercle(Point centre, double rayon)
        {
            Centre = centre;
            Rayon = rayon;
        }

        public bool Collision(Cercle c)
        {
            return Point.Distance(Centre, c.Centre) < Rayon + c.Rayon;
        }

        public bool Contient(Point p)
        {
            return Point.Distance(Centre, p) < Rayon;
        }
    }

}
