﻿namespace _05_poo
{
    // Classe partielle répartie sur plusieurs fichiers
    internal partial class Form1
    {

        private int a;

        public void Afficher()
        {
            Console.WriteLine(a);
        }
    }

}
