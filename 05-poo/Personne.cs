﻿namespace _05_poo
{
    internal class Personne
    {
        public string Prenom { get; set; }

        public string Nom { get; set; }

        public Personne(string prenom, string nom)
        {
            Prenom = prenom;
            Nom = nom;
        }

        public void Afficher()
        {
            Console.WriteLine($"{Prenom} {Nom}");
        }
    }
}
