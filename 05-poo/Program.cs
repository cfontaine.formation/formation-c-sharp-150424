﻿namespace _05_poo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Appel d'une méthode de classe
            Voiture.TestMethodeClasse();

            // Accès à une variable de classe
            // Console.WriteLine($"nombre de voiture={Voiture.compteurVoiture}); // private

            // Instantiation de la classe Voiture
            Voiture v1 = new Voiture();
            // Console.WriteLine($"nombre de voiture={Voiture.compteurVoiture}); 

            // Accès à une variable d'instance
            // v1.vitesse = 30
            // v1.Vitesse =30
            Console.WriteLine(v1.Vitesse);

            // Appel d’une méthode d’instance
            v1.Accelerer(20);
            v1.Afficher();
            v1.Freiner(40);
            Console.WriteLine(v1.EstArreter());
            v1.Arreter();
            Console.WriteLine(v1.EstArreter());
            v1.Afficher();

            Voiture v2 = new Voiture("Honda", "Gris", "ax-4056");
            //     Console.WriteLine(Voiture.compteurVoiture);
            //    v2.vitesse = 10;

            v2 = null;// En affectant, null à la références v2.Il n'y a plus de référence sur l'objet voiture
                      // Il sera détruit lors de la prochaine execution du garbage collector
                      // Appel explicite du garbage collector (à éviter)
                      //   GC.Collect();

            // Rappel Poo et propriété classe Article 
            Article article = new Article(10.0, "Cable RJ45", "Marque A");
            article.PrixHt = 15.0;
            Console.WriteLine(article.CalculPrixTTC(1.05));

            // Indexeur
            Phrase ph1 = new Phrase("Il", "fait", "beaux", "aujourd'hui");
            Console.WriteLine(ph1[2]);
            ph1[2] = "beau";
            Console.WriteLine(ph1["fait"]);

            // Méthode d'extension
            // ajouter des fonctionnalités à des classes existantes
            string strEx = "azerty";
            Console.WriteLine(strEx.Inverser());

            // Classe statique
            //  Math m = new Math();    // ne peut pas être instanciée
            // ne peut pas contenir de constructeurs d’instances
            Console.WriteLine(Math.Max(3, 4));    // contient uniquement des membres statiques

            // Agrégation
            Personne per1 = new Personne("John", "Doe");
            v1.Proprietaire = per1;
            v1.Afficher();

            Voiture v3 = new Voiture("bleu", "ford", "fr-2354-ze", per1);
            v3.Afficher();

            Voiture v4 = new Voiture("jaune", "fiat", "fr-2354-ze", new Personne("Jane", "Doe"));
            v4.Afficher();

            // Héritage
            VoiturePrioritaire vp1 = new VoiturePrioritaire();
            vp1.AllumerGyro();
            vp1.Afficher();

            // Classe Imbriquée
            Conteneur c = new Conteneur();
            c.Test();

            // Classe partielle
            Form1 form = new Form1(42);
            form.Afficher();


            #region Exercice CompteBancaire
            CompteBancaire cb1 = new CompteBancaire(100.0, per1);
            // cb1.solde = 100.0;
            // cb1.iban = "fr5962-0000";
            // cb1.titulaire = "John Doe";
            cb1.Afficher();
            cb1.Crediter(100.0);
            cb1.Afficher();
            cb1.Debiter(150.0);
            Console.WriteLine(cb1.EstPositif());
            cb1.Afficher();
            #endregion

            #region Exercice Héritage
            CompteEpargne ce = new CompteEpargne(0.5, per1);
            ce.Afficher();
            ce.CalculInterets();
            ce.Afficher();
            #endregion

            #region  Exercice Point 
            Point a = new Point();
            a.Afficher();
            a.Deplacer(3, 1);
            a.Afficher();
            Point b = new Point(5, 1);
            b.Afficher();
            Console.WriteLine($"Distance = {Point.Distance(a, b)}");
            #endregion

            #region Exercice Agrégation Cercle
            Cercle c1 = new Cercle(new Point(2, 0), 2);
            Cercle cc = new Cercle(new Point(6, 0), 3);
            Cercle cnc = new Cercle(new Point(10, 0), 1);
            Console.WriteLine(c1.Collision(cc));
            Console.WriteLine(c1.Collision(cnc));

            Point p1 = new Point(1, 1);
            Point p10 = new Point(10, 10);
            Console.WriteLine(c1.Contient(p1));
            Console.WriteLine(c1.Contient(p10));
            #endregion

        }
    }
}
