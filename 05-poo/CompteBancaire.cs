﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
    internal class CompteBancaire
    {
        public double Solde { get; protected set; }
        public string Iban { get; }
        public Personne Titulaire { get; set; }

        private static int numCompte;

        public CompteBancaire(Personne titulaire)
        {
            Titulaire = titulaire;
            numCompte++;
            Iban = "fr-5962-0000-" + numCompte;
        }

        public CompteBancaire(double solde, Personne titulaire) : this(titulaire)
        {
            Solde = solde;
        }

        public void Afficher()
        {
            Console.WriteLine("-----------------------");
            Console.WriteLine($"Solde={Solde}");
            Console.WriteLine($"Iban={Iban}");
            Console.Write($"Titulaire=");
            Titulaire?.Afficher();
            Console.WriteLine("-----------------------");
        }

        public void Crediter(double valeur)
        {
            if (valeur > 0)
            {
                Solde += valeur;
            }
        }

        public void Debiter(double valeur)
        {
            if (valeur > 0)
            {
                Solde -= valeur;
            }
        }

        public bool EstPositif()
        {
            return Solde >= 0;
        }
    }
}
