﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
    internal class CompteEpargne : CompteBancaire
    {
        public double Taux { get; set; }


        public CompteEpargne(double taux,Personne titulaire) : base(titulaire)
        {
            Taux = taux;
        }

        public CompteEpargne(double taux, double solde, Personne titulaire) : base(solde, titulaire)
        {
            Taux = taux;
        }

        public void CalculInterets()
        {
            Solde *= 1 + Taux / 100;
        }
    }
}
