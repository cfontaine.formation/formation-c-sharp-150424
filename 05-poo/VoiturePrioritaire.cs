﻿namespace _05_poo
{
    internal class VoiturePrioritaire : Voiture // VoiturePrioritaire hérite de Voiture
    {
        public bool Gyro { get; private set; }

        // base => pour appeler le constructeur de la classe mère
        public VoiturePrioritaire() // si on ne présise rien, le constructeur par défaut de la classe mère est appelé implicitement
        {                           // S'IL EXISTE DANS LA CLASSE PARENT, sinon on est obligé d'appeler un constructeur de la classe mère
                                    // explicitement avec base
            Console.WriteLine("Constructeur par defaut VoiturePrioritaire");
        }

        //  base(marque, couleur, plaque) => pour appeler le constructeur 3 paramètres de la classe mère
        public VoiturePrioritaire(string marque, string couleur, string plaque, bool gyro) : base(marque, couleur, plaque)
        {
            Console.WriteLine("Constructeur 4 paramètre de VoiturePrioritaire");
            Gyro = gyro;
        }

        public void AllumerGyro()
        {
            Gyro = true;
        }

        public void EteindreGyro()
        {
            Gyro = false;
        }

        // override -> Redéfinition d'une méthode virtual de la classe mère
        public override void Afficher()
        {
            base.Afficher();
            Console.WriteLine($"gyro={Gyro}");
        }

        // new -> : Occultation: redéfinir une méthode d'une classe mère et  « casser » le lien vers la classe mère
        public new void Accelerer(int vAcc)
        {
            Vitesse = vAcc * 2;
        }
    }
}
