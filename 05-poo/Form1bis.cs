﻿namespace _05_poo
{
    // Classe partielle répartie sur plusieurs fichiers
    internal partial class Form1
    {
        public Form1()
        {
        }
        public Form1(int a)
        {
            this.a = a;
        }
    }

}
