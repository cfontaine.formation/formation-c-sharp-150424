﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
    internal class Conteneur
    {
        string _vInstance = "instance";

        static string _vClasse = "classe";

        public void Test()
        {
            Element elm = new Element();
            elm.Afficher(this);
        }


        // classe imbriquée
        class Element
        {
            public void Afficher(Conteneur c)
            {
                Console.WriteLine(_vClasse);        // On a accès aux variables de classe de la classe Conteneur
                Console.WriteLine(c._vInstance);    // Pour accèder aux variables d'instance de la classe Conteneur
            }                                       // Il faut passer un objet conteneur en paramètre
        }

    }

}
