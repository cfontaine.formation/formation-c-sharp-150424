﻿namespace _05_poo
{
    internal class Voiture
    {

        // Variable d'instance
        // On n'a plus besoin de déclarer les variables d'instances _marque,_couleur,_compteurKm, _plaqueIma, elles seront générées automatiquement par les propriétées automatique

        // private string _marque;
        // private string _couleur = "Rouge";
        // private string _plaque;
        // private int _vitesse;
        // private int _compteurKm = 200;

        // Propriété
        // Propriété auto-implémenté => la variable d'instance est générée par le compilateur
        public string Marque { get; }
        public string Couleur { get; set; } = "Rouge";
        public string Plaque { get; set; }
        public int Vitesse { get; protected set; }
        public int CompteurKm { get; } = 200;   // On peut donner un valeur par défaut à une propriétée (littéral, expression ou une fonction)

        // Variable de classe -> partagé par toutes les instances
        // private static int compteurVoiture; // remplacer par une propriété statique
        public static int CompteurVoiture { get; private set; }

        // Agrégation
        public Personne Proprietaire { get; set; }


        // Encapsulation en java et en C++ -> on utilise des méthodes pour accéder aux variables d'instances
        //public int GetVitesse()
        //{
        //    return _vitesse;
        //}

        //public void SetVitesse(int vitesse)
        //{
        //    if (vitesse > 0)
        //    {
        //        _vitesse = vitesse;
        //    }
        //}

        // Encapsulation  en C# -> propriété
        //public int Vitesse
        //{
        //    get     // accès en lecture
        //    {
        //        return _vitesse;  //on retourne une valeur
        //    }
        //    set     // accès en écriture
        //    {
        //        if (value > 0)
        //        {
        //            _vitesse = value; // value permet de récupérer la valeur "affectée" à la propriété
        //        }
        //    }
        //}

        // à partir de  C# 7.0, On peut utiliser les corps d'expression dans une propriétée 
        //public string Couleur
        //{
        //    get => couleur;
        //    set => couleur = value;
        //}


        // Constructeur par défaut
        public Voiture()
        {
            Console.WriteLine("Constructeur Voiture par défaut");
            CompteurVoiture++;
        }

        // : this() => Chainnage de constructeur : appel du constructeur par défaut
        public Voiture(string marque, string couleur, string plaque) : this()
        {
            Console.WriteLine("Constructeur Voiture 3 paramètres");
            Marque = marque;
            Couleur = couleur;
            Plaque = plaque;
        }

        // : this(marque, couleur, plaque) => Chainnage de constructeur : appel du constructeur  Voiture(string marque, string couleur, string plaque)
        public Voiture(string marque, string couleur, string plaque, int vitesse) : this(marque, couleur, plaque)
        {
            Vitesse = vitesse;
        }

        public Voiture(string couleur, string marque, string plaque, Personne proprietaire) : this(couleur, marque, plaque)
        {
            Proprietaire = proprietaire;
        }

        // Destructeur
        //~Voiture()
        //{
        //    Console.WriteLine("Destructeur");
        //}

        // Constructeur static: appelé avant la création de la première instance ou le référencement d’un membre statique
        static Voiture()
        {
            Console.WriteLine("Constructeur static Voiture");
        }

        // Méthodes d'instances => comportement
        public void Accelerer(int vAcc)
        {
            if (vAcc > 0)
            {
                Vitesse += vAcc;
            }
        }

        public void Freiner(int vFrn)
        {
            if (vFrn > 0)
            {
                Vitesse -= vFrn;
            }
        }

        public void Arreter()
        {
            Vitesse = 0;
        }

        public bool EstArreter()
        {
            return Vitesse == 0;
        }

        public virtual void Afficher()
        {
            Console.WriteLine($"{Marque} {Couleur} {Plaque} {Vitesse} {CompteurKm}");
            // Si on appel une méthode sur une variable ou une propriété null, on aura une exception NullReferenceException  
            // Pour l'éviter :
            // - on peut tester si la propriété est null
            //if (Proprietaire != null)
            //{
            //    Proprietaire.Afficher();
            //}
            // ou utiliser l'opérateur null condition ?.
            Proprietaire?.Afficher();  // la méthode Afficher est executée que si Proprietaire est différent de null
        }

        // Méthode de classe
        public static void TestMethodeClasse()
        {
            Console.WriteLine("Méthode de classe");
            //Console.WriteLine(vitesse); // Dans une méthode de classe, on n'a pas accès à une variable d'instance
            //freiner(10);                // ou une méthode d'instance
            Console.WriteLine(CompteurVoiture);
        }

        // mais on peut accéder à une variable d'instance par l'intermédiare d'un objet passé en paramètre
        public static bool EgaliteVitesse(Voiture va, Voiture vb)
        {
            return va.Vitesse == vb.Vitesse;
        }
    }
}
