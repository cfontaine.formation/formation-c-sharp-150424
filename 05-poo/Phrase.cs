﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
    internal class Phrase
    {
        private string[] _mots = new string[10];

        public Phrase(params string[] mots)
        {
            if (mots.Length < 10) { 
                for(int i = 0;i<mots.Length; i++)
                {
                    _mots[i] = mots[i];
                }
            }
            // else{
            //      lancer une exception
            // }
        }

        // indexeur
        public string this[int index]
        {
            get
            {
                return _mots[index];
            }
            set
            {
                _mots[index] = value;
            }
        }

        // On peut déclarer plusieurs indexeurs, chacun avec des paramètres de différents types
        public int this[string mot]
        {
            get
            {
                for(int i=0;i<_mots.Length;i++)
                {
                    if (_mots[i] == mot)
                    {
                        return i;
                    }
                }
                // lancer une exception
                throw new IndexOutOfRangeException();
            }
        }
    }
}
