﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
    internal class Article
    {
        public double PrixHt { get; set; }

        public string Description { get; set; }

        public string Marque { get; }

        public Article(double prix, string description, string marque)
        {
            PrixHt = prix;
            Description = description;
            Marque = marque;
        }

        public double CalculPrixTTC(double tauxTaxe)
        {
            return PrixHt * tauxTaxe;
        }
    }
}
