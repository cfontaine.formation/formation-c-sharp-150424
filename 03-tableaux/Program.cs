﻿namespace _03_tableaux
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region tableau à une dimmension
            // déclaraton
            double[] t = new double[5];
            // Valeur d'initialisation des éléments du tableau
            // - entier -> 0
            // - double , float ou un decimal -> 0.0
            // - char -> '\u0000'
            // - boolean -> false
            // - type référence  -> null

            // Accèder à un élément du tableau
            Console.WriteLine(t[0]);
            t[1] = 3.14;
            Console.WriteLine(t[1]);

            // Si l'on essaye d'accéder à un élément en dehors du tableau -> IndexOutOfRangeException
            //  Console.WriteLine(t[200]);

            // Nombre d'élément du tableau
            Console.WriteLine(t.Length);

            // Parcourir complétement un tableau (avec un for)
            for (int i = 0; i < t.Length; i++)
            {
                Console.WriteLine($"t[{i}]={t[i]}");
            }

            // Parcourir complétement un tableau (foreach)
            // foreach(double d in t) // ou
            foreach (var d in t)    // var -> d est de type double
            {
                Console.WriteLine(d); // d uniquement en "lecture"
                                      // d = 10.6; // => Erreur ,on ne peut pas modifier d
            }

            // Déclarer et initaliser un tableau
            string[] tStr = { "hello", "world", "azerty", "bonjour" };
            Console.WriteLine(tStr.Length);
            foreach (var s in tStr)
            {
                Console.WriteLine(s);
            }

            // On peut utiliser une variable entière pour définir la taille du tableau
            int size = Convert.ToInt32(Console.ReadLine());
            int[] t2 = new int[size];
            Console.WriteLine(t2.Length);

            #endregion

            #region Exercice tableau

            #region  1 - Trouver la valeur maximale et la moyenne d’un tableau de 5 entier: -7, -6, -4, -8, -3
            // int[] tab = { -7, -6, -4, -8, -3 }; // 1
            #endregion

            #region   2 - Modifier le programme pour faire la saisie de la taille du tableau et saisir les éléments du tableau
            int tabSize = Convert.ToInt32(Console.ReadLine());
            int[] tab = new int[tabSize];
            for (int i = 0; i < tabSize; i++)
            {
                Console.Write($"tab[{i}]=");
                tab[i] = Convert.ToInt32(Console.ReadLine());
            }
            #endregion

            int max = tab[0];// ou int.MinValue;
            double somme = 0.0;
            foreach (var elm in tab)
            {
                if (elm > max)
                {
                    max = elm;
                }
                somme += elm;
            }
            double moyenne = somme / tab.Length;
            Console.WriteLine($"Maximum={max}");
            Console.WriteLine($"Moyenne={moyenne}");
            #endregion

            #region Tableau Multidimensions
            // Déclaration d'un tableau à 2 dimensions
            // Nombre maximum de dimmension 32
            int[,] tab2d = new int[3, 4];

            // Accès à un élémént d'un tableau à 2 dimensions
            tab2d[0, 1] = 42;
            Console.WriteLine(tab2d[2, 2]); //0

            Console.WriteLine($"Nombre élément du tableau= {tab2d.Length}"); // 12

            Console.WriteLine($"Nombre de ligne={tab2d.GetLength(0)}"); // 3

            Console.WriteLine($"Nombre de colonne={tab2d.GetLength(1)}"); // 4

            Console.WriteLine($"Nombre de dimmension={tab2d.Rank}"); // 2

            // Parcourir complétement un tableau à 2 dimension => for
            for (int l = 0; l < tab2d.GetLength(0); l++)
            {
                for (int c = 0; c < tab2d.GetLength(1); c++)
                {
                    Console.Write($"{tab2d[l, c]}\t");
                }
                Console.WriteLine();
            }

            // Parcourir complétement un tableau à 2 dimension => foreach
            foreach (var e in tab2d)
            {
                Console.WriteLine(e);
            }

            // Déclaration et initialisation tableau à 2 dimensions
            char[,] tabChr2d = { { 'a', 'z', 'e' }, { 't', 'y', 'u' } };
            foreach (var elm in tabChr2d)
            {
                Console.WriteLine(elm);
            }

            // Déclarer un tableau à 3 dimmensions
            char[,,] tabChr3d = new char[4, 2, 6];
            #endregion

            #region Tableau en escalier
            // Déclaration d'un tableau de tableau
            double[][] tabEsc = new double[3][];
            tabEsc[0] = new double[4];
            tabEsc[1] = new double[2];
            tabEsc[2] = new double[3];

            // Accès à un élément
            tabEsc[1][1] = 123;
            Console.WriteLine(tabEsc[1][1]); // 0

            Console.WriteLine($"Nombre de ligne ={tabEsc.Length}");

            Console.WriteLine($"Nombre d'élément de la première ligne={tabEsc[0].Length}");    // -> 4
            // Nombre de colonne par ligne
            for (int i = 0; i < tabEsc.Length; i++)
            {
                Console.WriteLine(tabEsc[i].Length);
            }

            // Parcourir complétement un tableau de tableau => for
            for (int l = 0; l < tabEsc.Length; l++)
            {
                for (int c = 0; c < tabEsc[l].Length; c++)
                {
                    Console.Write($"{tabEsc[l][c]}\t");
                }
                Console.WriteLine();
            }

            // Parcourir complétement un tableau de tableau => foreach
            foreach (double[] row in tabEsc) // ou var
            {
                foreach (double elm in row) // ou var
                {
                    Console.Write($"{elm}\t");
                }
                Console.WriteLine();
            }

            // Déclarer et initialiser un tableau de tableau
            int[][] tabEsc2 = new int[][]
            {
                new int[]{1,4,7 },
                new int[]{3,1 },
                new int[]{6,3,9,8,10 }
            };

            foreach (int[] row in tabEsc2) // ou var
            {
                foreach (int elm in row) // ou var
                {
                    Console.Write($"{elm}\t");
                }
                Console.WriteLine();
            }
            #endregion
        }
    }
}
