﻿namespace _06_polymorphisme
{
    internal interface IPeutVoler
    {
        void Atterir();
        void Decoller();
    }

}
