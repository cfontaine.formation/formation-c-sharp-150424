﻿namespace _06_polymorphisme
{
    internal class Program
    {
        static void Main(string[] args)
        {
/*            Animal a1 = new Animal(2, 4000);
            a1.EmettreSon();*/

            Chien ch1 = new Chien(5, 7000, "rolo");
            ch1.EmettreSon();

            // upcasting implicite
            Animal a2 = new Chien(3, 2500, "laika");// On peut créer une instance de Chien (classe fille) mais est référencée a une référence, une référence Animal (classe mère)
                                                    // L'objet Chien sera vu comme un Animal, on ne pourra pas accèder aux propriétés et aux méthodes propre au chien (Nom,...)
                                                    // Comme la méthode est virtual dans Animal et si elle est rédéfinie dans Chien, c'est la méthode de Chien qui sera appelée


            a2.EmettreSon();

            // downcasting -> convertion explicite
            // Chien ch2 = (Chien) a1;
            // as équivalent de (cast) pour les objet 
            if(a2 is Chien)  // test si a2 est de "type" Chien
            {
                Chien ch2 = a2 as Chien;
                Console.WriteLine(ch2.Nom);
            }

            if(a2 is Chien ch6) // Avec l'opérateur is, on peut tester si a2 est de type Chien et faire la conversion en même temps
            {
                Console.WriteLine(ch6.Nom);
            }
         
            Animalerie n= new Animalerie();
            n.Ajouter(new Chien(5, 7000, "Rolo"));
            n.Ajouter(new Chien(5, 4000, "Idefix"));
            n.Ajouter(new Chat(5,2000,7));
            n.Ecouter();

            //Object
            // ToString
            Console.WriteLine(a2);
            // Equals
            Chien ch4 = new Chien(5, 7000, "Rolo");
            Chien ch5 = new Chien(5, 7000, "Rolo");
            // si l'opérateur == n'est pas redéfinit, on compare l'égalité des références comme se sont 2 objets différents -> false
            Console.WriteLine(ch4==ch5); // false
            // la méthode Equals de object, compare aussi les références des 2 objets, il faut la redéfinir pour choisir se qui détermine l'égalité des objet 
            Console.WriteLine(ch4.Equals(ch5)); // si Equal a été redéfinie -> true

            // => pour tester l'égalité des objets, il faut utiliser Equals ou si on a re définie l'opérateur ==

            // Interface
            // On peut utiliser une référence vers une interface pour référencer une classe qui l'implémente
            IPeutMarcher ipm = new Chien(1, 2000, "Tom");
            ipm.Marcher();
        }
    }
}
