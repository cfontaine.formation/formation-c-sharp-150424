﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_polymorphisme
{

    internal class Canard : Animal, IPeutMarcher, IPeutVoler  // On peut implémenter plusieurs d'interface
    {
        public Canard(int age, int poid) : base(age, poid)
        {
        }

        public override void EmettreSon()
        {
            Console.WriteLine("Coin coin");
        }

        public void Marcher()
        {
            Console.WriteLine(" Le canard marche");
        }

        public void Courrir()
        {
            Console.WriteLine(" Le carnard court");
        }

        public void Atterir()
        {
            Console.WriteLine(" Le canard atterit");
        }

        public void Decoller()
        {
            Console.WriteLine(" Le canard décole");
        }
    }
}

