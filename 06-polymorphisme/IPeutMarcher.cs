﻿namespace _06_polymorphisme
{
    // Un interface ne définit que des méthodes qui deveront être obligatoirement implémenté dans la classe qui va impléménter l'interface
    // Un interface commence toujours par convention par I

    internal interface IPeutMarcher
    {
        void Marcher();

        void Courrir();
    }
}
