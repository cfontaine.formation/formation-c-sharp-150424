﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_polymorphisme
{
    internal class Chien : Animal, IPeutMarcher // La classe Chien hérite de la classe Animal et implémente l'interface IPeutMarcher
    {
        public string Nom { get; set; }
        public Chien(int age, int poid, string nom) : base(age, poid)
        {
            Nom = nom;
        }

        // Redéfinition obligatoire de la méthode abstraite EmmettreUnSon de la classe Animal 
        // sealed sur une méthode interdit la redéfiniton de la méthode dans les sous-classes

        public sealed override void EmettreSon()
        {
            Console.WriteLine($"{Nom} aboie");
        }
        public void Marcher()   // Impléméntation de la méthode Marcher de l'interface IPeutMarcher
        {
            Console.WriteLine($"{Nom} marche");
        }

        public void Courrir()   // Impléméntation de la méthode Courrir de l'interface  IPeutMarcher
        {
            Console.WriteLine($"{Nom} court");
        }

        public override string ToString()
        {
            return $"Chien[ {Nom}, {Poid}, {Age}]"; ;
        }

        public override bool Equals(object? obj)
        {
            return obj is Chien chien &&
                   Age == chien.Age &&
                   Poid == chien.Poid &&
                   Nom == chien.Nom;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Age, Poid, Nom);
        }
    }
}
