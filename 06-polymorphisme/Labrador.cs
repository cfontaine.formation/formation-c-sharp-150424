﻿namespace _06_polymorphisme
{
    internal class Labrador : Chien
    {

        public Labrador(double poid, int age, string nom) : base(poid, age, nom)
        {
        }

        // Comme la méthode est sealed dans la classe mère, on ne peut plus redéfinir la méthode
        //public override void EmettreSon()
        //{
        //    Console.WriteLine("Il prle");
        //}

    }

}
