namespace _14_exempleWinform
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult res=MessageBox.Show("Voulez ajouter?","Annuaire",MessageBoxButtons.OKCancel,MessageBoxIcon.Question);
            if(res==DialogResult.OK)
            {
                Close();
            }
        }
    }
}
