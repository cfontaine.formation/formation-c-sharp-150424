﻿
using _13_libBddMySql;
using System.Configuration;
using System.Windows;

namespace _18_Annuaire_WPF
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ContactDao dao = new ContactDao();

        public MainWindow()
        {
            InitializeComponent();
            ContactDao.ChaineConnection = "Server=localhost;Port=3306;Database=formation_c;Uid=root;Pwd=;SslMode=none";
            ContactGrid.ItemsSource = dao.FindAll();
        }

        private void OnClickBpQuitter(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void OnClickBpAjouter(object sender, RoutedEventArgs e)
        {
            Contact c = SaisirContact("Ajouter un contact");
            if (c != null)
            {
                dao.SaveOrUpdate(c, false);
                ContactGrid.ItemsSource = dao.FindAll();
            }
        }

        private void OnClickBpSupprimer(object sender, RoutedEventArgs e)
        {
            if (IsSelectedContact("Effacer un contact"))
            {
                Contact contact = ContactGrid.SelectedItem as Contact;
                dao.Remove(contact.Id);
            }
            ContactGrid.ItemsSource = dao.FindAll();
        }

        private void OnClickBpModifier(object sender, RoutedEventArgs e)
        {
            if (IsSelectedContact("Modifier un contact"))
            {
                Contact c = SaisirContact("Modifier un contact", ContactGrid.SelectedItem as Contact);
                if (c != null)
                {
                    dao.SaveOrUpdate(c, false);
                    ContactGrid.ItemsSource = dao.FindAll();
                }
            }
        }

        private Contact SaisirContact(string titre = "", Contact c = null)
        {
            Contact rc = null;
            ContactEditWindow ce = new ContactEditWindow(c);
            ce.Owner = this;
            if (ce.ShowDialog() == true)
            {
                Contact search = dao.FindByEmail(ce.ContactForm.Email);
                if ((search != null && c == null) || (search != null && search.Id != c.Id))
                {
                    MessageBox.Show("L'email existe déjà", titre, MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    rc = ce.ContactForm;
                }
            }
            ce.Close();
            return rc;
        }

        private bool IsSelectedContact(string titre = "")
        {
            if (ContactGrid.SelectedIndex == -1)
            {
                MessageBox.Show("Il n'a pas de ligne sélectionner", titre, MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            return true;
        }

    }
}
