﻿namespace _06bis_exercice_polymorphisme
{
    enum Couleurs { VERT, ROUGE, BLEU, ORANGE }
    internal abstract class Forme
    {
        public Couleurs Couleur { get; set; }

        public Forme(Couleurs couleur)
        {
            Couleur = couleur;
        }

        public abstract double CalculSurface();

        public override string ToString()
        {
            return $"Forme[${Couleur}]";
        }
    }

}
