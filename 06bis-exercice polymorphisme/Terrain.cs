﻿namespace _06bis_exercice_polymorphisme
{
    internal class Terrain
    {
        Forme[] _formes = new Forme[10];

        int nbForme;

        public void Ajouter(Forme forme)
        {
            if (nbForme < _formes.Length)
            {
                _formes[nbForme] = forme;
                nbForme++;
            }
            else
            {
                throw new IndexOutOfRangeException("Nombre maximum de forme atteint");
            }
        }

        public double CalculSurface()
        {
            double surface = 0;
            for (int i = 0; i < nbForme; i++)
            {
                surface += _formes[i].CalculSurface();
            }
            return surface;
        }

        public double CalculSurface(Couleurs c)
        {
            double surface = 0;
            for (int i = 0; i < nbForme; i++)
            {
                if (_formes[i].Couleur == c)
                {
                    surface += _formes[i].CalculSurface();
                }
            }
            return surface;
        }
    }
}
