﻿namespace _06bis_exercice_polymorphisme
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Terrain terrain = new Terrain();
            terrain.Ajouter(new Cercle(1.0, Couleurs.VERT));
            terrain.Ajouter(new Cercle(1.0, Couleurs.VERT));
            terrain.Ajouter(new Rectangle(2.0, 1.0, Couleurs.BLEU));
            terrain.Ajouter(new Rectangle(2.0, 1.0, Couleurs.BLEU));
            terrain.Ajouter(new TriangleRectangle(1.0, 1.0, Couleurs.ROUGE));
            terrain.Ajouter(new TriangleRectangle(1.0, 1.0, Couleurs.ROUGE));
            terrain.Ajouter(new TriangleRectangle(1.0, 1.0, Couleurs.ORANGE));
            terrain.Ajouter(new Rectangle(2.0, 1.0, Couleurs.ORANGE));

            Console.WriteLine($"Surface total= {terrain.CalculSurface()}");
            Console.WriteLine($"Surface Vert= {terrain.CalculSurface(Couleurs.VERT)}");
            Console.WriteLine($"Surface Bleu= {terrain.CalculSurface(Couleurs.BLEU)}");
            Console.WriteLine($"Surface Rouge= {terrain.CalculSurface(Couleurs.ROUGE)}");
            Console.WriteLine($"Surface Orange= {terrain.CalculSurface(Couleurs.ORANGE)}");
        }
    }
}
