﻿namespace _06bis_exercice_polymorphisme
{
    internal class TriangleRectangle : Rectangle
    {
        public TriangleRectangle(double largeur, double longueur, Couleurs couleur) : base(largeur, longueur, couleur)
        {

        }
        public override double CalculSurface()
        {
            return base.CalculSurface() / 2.0;
        }
    }

}
