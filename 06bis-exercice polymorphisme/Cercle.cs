﻿namespace _06bis_exercice_polymorphisme
{
    class Cercle : Forme
    {
        public double Rayon { get; set; }

        public Cercle(double rayon, Couleurs couleur) : base(couleur)
        {
            Rayon = rayon;
        }

        public override double CalculSurface()
        {
            return Math.PI * Math.Pow(Rayon, 2);
        }
    }
}
