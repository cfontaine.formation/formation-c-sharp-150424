﻿namespace _06bis_exercice_polymorphisme
{
    internal class Rectangle : Forme
    {
        public double Largeur { get; set; }
        public double Longueur { get; set; }
        public Rectangle(double largeur, double longueur, Couleurs couleur) : base(couleur)
        {
            Largeur = largeur;
            Longueur = longueur;
        }

        public override double CalculSurface()
        {
            return Largeur * Longueur;
        }
    }

}
