﻿using System.ComponentModel;

namespace _02_instructions
{
    internal class Program
    {
        static void Main(string[] args)
        {

            #region Condition if
            Console.WriteLine("Saisir un nombre entier");
            int v = Convert.ToInt32(Console.ReadLine());
            if (v > 100)
            {
                Console.WriteLine("la valeur saisie est supérieur à 100");
            }
            else if (v == 100)
            {
                Console.WriteLine("la valeur saisie est égale à 100");
            }
            else
            {
                Console.WriteLine("la valeur saisie est inférieure à 100");
            }
            #endregion

            #region Exercice: trie de 2 valeurs
            // Saisir 2 nombres et afficher ces nombres triés dans l'ordre croissant sous la forme 1.5 < 10.5
            Console.WriteLine("Trie: saisir 2 entiers ");
            double d1 = Convert.ToDouble(Console.ReadLine());
            double d2 = Convert.ToDouble(Console.ReadLine());
            if (d1 < d2)
            {
                Console.WriteLine($"{d1}<{d2}");
            }
            else
            {
                Console.WriteLine($"{d2}<{d1}");
            }
            #endregion

            #region Exercice: Intervalle
            // Saisir un nombre et dire s'il fait parti de l'intervalle - 4(exclu) et 7(inclu)
            Console.Write("Intervalle: Saisir un entier ");
            int val = Convert.ToInt32(Console.ReadLine());
            if (val > -4 && val <= 7)
            {
                Console.WriteLine($"{val} fait partie de l'intervale");
            }
            #endregion

            #region Opérateur ternaire
            Console.Write("Valeur absolue: Saisir une valeur double ");
            int va = Convert.ToInt32(Console.ReadLine());
            int vabs = va > 0 ? va : -va;
            Console.WriteLine($"|{va}|={vabs}");
            #endregion

            #region condition switch

            const int PREMIER_JOUR = 1;
            Console.Write("Saisir un jour entre 1 et 7 ");
            int jour = Convert.ToInt32(Console.ReadLine());
            switch (jour)
            {
                // la valeur d'un case doit être une constante au moment de la compilation
                case PREMIER_JOUR:    // cela peut être une constante
                case PREMIER_JOUR + 1:
                case 3:               // ou une littéral
                case 4:
                case 5:
                    Console.WriteLine("Semaine");
                    break;
                case 6:
                case 7:
                    Console.WriteLine("Week-end");
                    break;
                default:
                    Console.WriteLine("Erreur de saisie");
                    break;
            }

            // Clause when C# 7.0 
            switch (jour)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    break;
                case var x when x == 6 || x == 7:
                    Console.WriteLine("week-end");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            // switch avec les corps d'expression C# 8.0 (.net 6 et .net 8)
            string nomJour = jour switch
            {
                1 => "Lundi",
                6 => "samedi",
                _ => "Autre jour"
            };
            Console.WriteLine(nomJour);

            #endregion
            #region exercice calculatrice
            // Exercice: Calculatrice
            // Faire un programme calculatrice
            // Saisir dans la console
            // - un nombre à virgule flottante v1
            // - une chaîne de caractère opérateur qui a pour valeur valide:  + - * /
            // - un nombre à virgule flottante v2
            // Afficher:
            // - Le résultat de l’opération
            // - Un message d’erreur si l’opérateur est incorrect
            // - Un message d’erreur si l’on fait une division par 0

            Console.WriteLine("Calculatrice: saisir un double, un opérateur + - * / et un double");
            double v1 = Convert.ToDouble(Console.ReadLine());
            string op = Console.ReadLine();
            double v2 = Convert.ToDouble(Console.ReadLine());

            switch (op)
            {
                case "+":
                    Console.WriteLine($" {v1} + {v2} = {v1 + v2}");
                    break;
                case "-":
                    Console.WriteLine($" {v1} - {v2} = {v1 - v2}");
                    break;
                case "*":
                    Console.WriteLine($" {v1} * {v2} = {v1 * v2}");
                    break;
                case "/":
                    if (v2 == 0.0)  // (val2>-1e-9 && val2<1e-9)
                    {
                        Console.WriteLine("division par 0");
                    }
                    else
                    {
                        Console.WriteLine($" {v1} / {v2} = {v1 / v2}");
                    }
                    break;
                default:
                    Console.WriteLine($"L'opérateur {op} n'est pas correct");
                    break;
            }
            #endregion

            #region boucles
            // while
            int j = 0;
            while (j < 10)
            {
                Console.WriteLine(j);
                j++;
            }
            j = 0;

            // do .. while
            do
            {
                Console.WriteLine(j);
                j++;
            } while (j < 10);

            // for
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }

            #region break
            for (int i = 0; i < 10; i++)
            {
                if (i == 2)
                {
                    break;  // break => termine la boucle
                }
                Console.WriteLine(i);
            }
            #endregion

            #region continue
            for (int i = 0; i < 10; i++)
            {
                if (i == 2)
                {
                    continue;   // continue => on passe à l'itération suivante
                }
                Console.WriteLine(i);
            }
            #endregion

            // goto pour quitter des boucles imbriquées
            for (int i = 0; i < 10; i++)
            {
                for (int k = 0; k < 10; k++)
                {
                    if (i == 2)
                    {
                        goto EXIT_LOOP;
                    }
                    Console.WriteLine($"{i} {k}");
                }
            }
        EXIT_LOOP:

            // goto avec un switch
            int jo = 5;
            switch (jo)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    goto default;   // on execute aussi default
                case 2:
                    Console.WriteLine("Mardi");
                    break;
                default:
                    Console.WriteLine("Autre jour");
                    goto case 2;    // on execute aussi case = 2
            }
            #endregion
            #region Exercice boucle
            //Table de multiplication
            //Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9

            //    1 X 4 = 4
            //    2 X 4 = 8
            //    …
            //    9 x 4 = 36

            //Si le nombre passé en paramètre est en dehors de l’intervalle 1 à 9 on arrête sinon on redemande une nouvelle valeur
            Console.WriteLine("Saisir un entier entre 1 et 9 pour afficher sa table de multiplication");
            for (; ; ) //ou  while(true)
            {
                int m = Convert.ToInt32(Console.ReadLine());
                if (m < 1 || m > 9)
                {
                    break;
                }
                for (int i = 1; i < 9; i++)
                {
                    Console.WriteLine($"{i} x {m} ={i * m}");
                }
            }

            // Quadrillage 
            // Créer un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne
            //    ex: pour 2 3
            //    [ ][ ]
            //    [ ][ ]
            //    [ ][ ]

            int col = Convert.ToInt32(Console.ReadLine());
            int ligne = Convert.ToInt32(Console.ReadLine());
            for (int l = 0; l < ligne; l++)
            {
                for (int c = 0; c < col; c++)
                {
                    Console.Write("[ ]");
                }
                Console.WriteLine();
            }
            #endregion
        }
    }
}
