﻿using System.Text;

namespace _01_base
{
    // Une enumération est un ensemble de constante
    // Par défaut, une énumération est de type int , on peut définir un autre type qui ne peut-être que de type intégral
    enum Motorisation : sbyte { ESSENCE = 95, DIESEL = 30, GPL = 50, ELECTRIQUE = 2, ETHANOL = 60 }

    // Énumération comme indicateurs binaires
    [Flags]
    enum JourSemaine
    {
        LUNDI = 1,
        MARDI = 2,
        MERCREDI = 4,
        JEUDI = 8,
        VENDREDI = 16,
        SAMEDI = 32,
        DIMANCHE = 64,
        WEEKEND = SAMEDI | DIMANCHE
    }

    // Définition d'une structure
    struct Point
    {
        public int x;
        public int y;
    };
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Déclaration variable

            // Déclaration d'une variable   type nomVariable;
            int j;

            //Console.WriteLine(i); // Erreur => on ne peut pas utiliser une variable locale non initialisée

            // Initialisation    nomVariable = valeur
            j = 42;
            Console.WriteLine(j);

            // Déclaration et initialisation de variable 
            int k = 34;
            Console.WriteLine(k);

            // Déclaration multiple de variable
            double largeur = 4.5, hauteur = 34.5;
            Console.WriteLine(largeur + " " + hauteur); // + => concaténation
            #endregion

            #region Littéral

            // Littéral booléen
            bool tst = false; // ou true
            Console.WriteLine(tst);

            // Littéral caractère
            char chr = 'a';
            char chrUnicode = '\u0045';     // Caractère en UTF-16
            char chrUnicodeHexa = '\x45';
            Console.WriteLine(chrUnicodeHexa + " " + chrUnicode + " " + chr);

            // Littéral entier -> int par défaut
            long m = 123L;  // L > long
            uint ui = 123U; // U -> unsigned
            Console.WriteLine(m + " " + ui);

            // Littéral entier -> changement de base
            int dec = 123;      // décimal (base 10) par défaut
            int hexa = 0xF2E;   // 0x => héxadécimal (base 16)
            int bin = 0b010110; // 0b => binaire (base 2)
            Console.WriteLine(dec + " " + bin + " " + hexa);

            // Littéral nombre à virgule flottante
            double d1 = 12.3;
            double d2 = .5;
            double d3 = 1.234e3;
            Console.WriteLine(d1 + " " + d2 + " " + d3);

            // Littéral nombre à virgule flottante -> par défaut double
            float f = 12.3F;        // F -> float
            decimal deci = 12.3M;   // M -> Decimal
            Console.WriteLine(f + " " + deci);
            #endregion

            #region Typage implicite
            // Type implicite -> var
            // Le type est déterminé par le type de la littérale, de l'expression ou du retour d'une méthode
            var v1 = 10.5; // v1 -> double
            var v2 = 3.4M; // v2 -> decimal
            var v3 = j + k; // v3 -> int
            var v4 = "hello"; // v4 -> string
            Console.WriteLine(v1 + " " + v2 + " " + v3 + " " + v4);
            #endregion

            // avec @ on peut utiliser les mots réservés comme nom de variable (uniquement si nécessaire)
            int @while = 1;

            #region transtypage
            // Transtypage autormatique (pas de perte de donnée)
            // type inférieur => type supérieur
            int ti1 = 45;
            double ti2 = ti1;
            long ti3 = ti1;

            // Transtypage explicite: cast -> (nouveauType)
            double te1 = 3.4;
            int te2 = (int)te1;
            float te3 = (float)te1;
            decimal te4 = (decimal)te1;

            // Dépassement de capacité
            // int dep1 = 300;            // 00000000 00000000 00000001 00101100    300
            // sbyte dep2 = (sbyte)dep1;  //                            00101100    44
            // Console.WriteLine(dep1 + " " + dep2);

            // checked => checked permet d’activer explicitement le contrôle de dépassement de capacité 
            // pour les conversions et les opérations arithmétiques de type intégral
            //checked
            //{
            //    dep2 = (sbyte)dep1; // avec checked une exception est générée, s'il y a un dépassement de capacité
            //}

            // Par défaut la vérification des dépassement de capacités est unchecked
            // si l'on veut qu'elles soient par défaut checked: il faut choisir l'option checked dans les propriétés du projet dans Advanced Build Settings
            // Dans ce cas, on utilise unchecked pour désactiver la vérification des dépassements
            //unchecked
            //{
            //    dep2 = (sbyte)dep1;  // plus de vérification de dépassement de capacité
            //}
            //Console.WriteLine(dep1 + " " + dep2);
            #endregion

            #region fonction de conversion
            // La classe Convert contient des méthodes statiques permettant de convertir un entier vers un double, ...
            // On peut aussi convertir une chaine de caractères en un type numérique
            int fc1 = Convert.ToInt32("1234");
            Console.WriteLine(fc1);
            double fc2 = Convert.ToDouble("1,2");
            Console.WriteLine(fc2);

            // int fc = Convert.ToInt32("azerty"); //  Erreur => génère une exception formatException

            // Conversion d'une chaine de caractères en entier
            // Parse
            int fc3 = int.Parse("345");
            //fc3 = int.Parse("azery");     //  Erreur => génère une exception formatException
            Console.WriteLine(fc3);

            // TryParse
            int fc4;
            bool tstCnv = int.TryParse("123", out fc4); // Retourne true et la convertion est affecté à fc4
            Console.WriteLine(fc4 + " " + tstCnv);

            tstCnv = int.TryParse("345z", out fc4); // Retourne false, 0 est affecté à fc4
            Console.WriteLine(fc4 + " " + tstCnv);
            #endregion

            #region Type référence
            StringBuilder sb1 = new StringBuilder("azerty");
            StringBuilder sb2 = null;
            Console.WriteLine(sb1 + " " + sb2);
            sb2 = sb1;
            Console.WriteLine(sb1 + " " + sb2);
            sb1 = null;
            Console.WriteLine(sb1 + " " + sb2);
            sb2 = null; // s1 et s2 sont égales à null
                        // Il n'y a plus de référence sur l'objet, il est éligible à la destruction par le garbage collector
            Console.WriteLine(sb1 + " " + sb2);
            #endregion

            #region Constante
            const double PI = 3.14;
            Console.WriteLine(PI);
            double pi2 = PI * 2;
            Console.WriteLine(pi2);
            // PI = 3.1419;     // Erreur: on ne peut pas modifier la valeur d'une constante
            //const int vr1;    // Erreur: on est obligé d'initialiser une constante
            #endregion

            #region opérateur
            // opérateur arithmétique
            int a1 = 3;
            int a2 = 32;
            int r = a1 + a2;
            Console.WriteLine(r);    // 35
            // % -> modulo: reste de la division entière
            Console.WriteLine(a1 % 2); // 1

            // division par 0

            // entier / 0 -> exception
            // int zero = 0;
            // Console.WriteLine(1/zero); 

            // Nombre à virgule flottante
            Console.WriteLine(1.0 / 0.0); // double.PositiveInfinity
            Console.WriteLine(-1.0 / 0.0); // double.NegativeInfinity
            Console.WriteLine(0.0 / 0.0); // double.NaN

            // Incrémentation

            // pré-incrémentation
            int inc = 0;
            int res = ++inc;// inc=1 res=1
            Console.WriteLine(inc + " " + res);

            // post-incrémentation
            inc = 0;
            res = inc++; // res =0 inc=1
            Console.WriteLine(inc + " " + res);

            // Affectation composé
            res = 12;
            res += 23; // conrespont à res = res + 23

            // opérateur de comparaison
            bool tst1 = res < 100;  // Une comparaison a pour résultat un booléen
                                    // true
                                    // Opérateur logique
                                    // Opérateur Non -> !
            Console.WriteLine(tst1 + " " + !tst1);

            // a  b | et ou   ou exclusif
            //---------------------------   
            // f  f | f | f | f
            // v  f | f | v | v
            // f  v | f | v | v
            // v  v | v | v | f

            // Opérateur court-circuit && et ||
            // && -> dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
            bool test = res > 100 && res++ == 35;   //  res++ n'est pas exécuté
            Console.WriteLine(test + " " + res);    // false 35

            // || -> dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
            test = res == 35 || res++ > 100;        // res++ n'est pas exécuté
            Console.WriteLine(test + " " + res);    // true 35

            // Opérateur Binaires (bit à bit)
            byte b = 0b10110;
            Console.WriteLine(Convert.ToString(~b, 2)); // Opérateur ~ => complémént: 1-> 0 et 0-> 1
            Console.WriteLine(Convert.ToString(b & 0b1101, 2)); // et -> 0100
            Console.WriteLine(Convert.ToString(b | 0b1101, 2)); // ou -> 11111
            Console.WriteLine(Convert.ToString(b ^ 0b1101, 2)); // ^ -> 11011

            // Opérateur de décallage
            Console.WriteLine(Convert.ToString(b << 3, 2)); // Décalage à gauche de 3 (insertion de trois 0 à droite)  => 10110000
            Console.WriteLine(Convert.ToString(b >> 2, 2)); // Décalage à droite de 2 (insertion de deux 0 à gauche) => 101

            // L’opérateur de fusion null ?? retourne la valeur de l’opérande de gauche si elle n’est pas null
            // sinon, il évalue l’opérande de droite et retourne son résultat
            string str = "hello";
            string resStr = str ?? "valeur par défaut";
            Console.WriteLine(resStr);  // hello

            str = null;
            resStr = str ?? "valeur par défaut";
            Console.WriteLine(resStr);  // valeur par défaut
            #endregion

            #region promotion numérique
            // 1) Le type le + petit est promu vers le + grand type des deux
            int pn1 = 11;
            long pn2 = 12L;
            long pnRes2 = pn1 + pn2; // pr1 est promu en long

            double pn3 = 4.5;
            double pnRes = pn1 + pn3; // pr1 est promu en double

            pnRes = pn1 / 2;            // un entier divisé par un entier donne un entier
            Console.WriteLine(pnRes);   // 5.0

            pnRes = pn1 / 2.0;          // on divise par double, pn1 est promu en double => la résultat est un double
            Console.WriteLine(pnRes);   // 5.5
            pnRes = ((double)pn1) / 2; // idem en convertissant avec un cast pr1 vers un double, 2 est promu en double 2.0
            Console.WriteLine(pnRes);

            // 2) sbyte, byte, short, ushort, char sont promus en int
            short sh1 = 1;
            short sh2 = 2;
            int sh3 = sh1 + sh2; // sh1 et sh2 sont promu en int
            Console.WriteLine(sh3);
            #endregion

            #region format chaine de caractère
            int x1 = 4;
            int y1 = 6;
            // 1
            Console.WriteLine(string.Format("x1={0},y1={1}", x1, y1));

            // On peut définir directement le format dans la mèthode WriteLine
            Console.WriteLine("x1={0},y1={1}", x1, y1);

            // 2
            Console.WriteLine($"x1={x1},y1={y1}  x1+y1={x1 + y1}");

            // 3
            Console.WriteLine("c:\tmp\newfile.txt");    // \t et \n sont considérés comme des caractères spéciaux

            // Caractères spéciaux
            // \n       Nouvelle ligne
            // \r       Retour chariot
            // \f       Saut de page
            // \t       Tabulation horizontale
            // \v       Tabulation verticale
            // \0       Caractère nul
            // \a 	    Alerte
            // \b       Retour arrière
            // \\       Backslash
            // \'       Apostrophe
            // \"       Guillemet

            Console.WriteLine(@"c:\tmp\newfile.txt"); // @ devant la littérale supprime l'interprétation dans la chaine des caractères spéciaux 

            Console.WriteLine("c:\\tmp\\newfile.txt"); // on peut arriver au même resultat en doublant les antislash

            #endregion
            #region Exercice Somme
            // Saisir 2 chiffres et afficher le résultat dans la console sous la forme 1 + 3 =  4
            Console.WriteLine("Somme: entrer 2 entiers");
            int s1 = Convert.ToInt32(Console.ReadLine());
            int s2 = Convert.ToInt32(Console.ReadLine());
            int somme = s1 + s2;
            Console.WriteLine(somme);
            Console.WriteLine($"{s1}+{s2}={somme}");
            #endregion

            #region Exercice Moyenne
            // Saisir 2 nombres entiers et afficher la moyenne dans la console
            Console.WriteLine("Moyenne: entrer 2 entiers");
            int m1 = Convert.ToInt32(Console.ReadLine());
            int m2 = Convert.ToInt32(Console.ReadLine());
            double moyenne = (m1 + m2) / 2.0;
            Console.WriteLine($"moyenne={moyenne}");
            #endregion

            #region Enumération
            // mt est une variable qui ne pourra accepter que les valeurs de l'enum Motorisation
            Motorisation mt = Motorisation.ESSENCE;
            Console.WriteLine(mt);

            // enum -> string (implicite)
            string mtStr = mt.ToString();
            Console.WriteLine(mtStr);

            // enum -> entier (cast)
            int iStr = (int)mt;
            Console.WriteLine(iStr);

            // string -> enum
            // La méthode Parse permet de convertir une chaine de caractère en une constante enummérée
            string m2str = "GPL";
            Motorisation mt2 = (Motorisation)Enum.Parse(typeof(Motorisation), m2str);
            Console.WriteLine(mt2);

            // entier -> enum
            sbyte vm = 95;
            // Il faut de tester, si la valeur entière existe dans l'enumération
            if (Enum.IsDefined(typeof(Motorisation), vm))
            {
                Motorisation m3 = (Motorisation)vm;
                Console.WriteLine(m3);
            }

            // Énumération comme indicateurs binaires
            JourSemaine rdv = JourSemaine.LUNDI | JourSemaine.MERCREDI;

            // Avec l'attribut [Flag] -> affiche LUNDI, MERCREDI , sans -> affiche 5
            Console.WriteLine(rdv);

            // teste la présence de LUNDI
            if ((rdv & JourSemaine.LUNDI) != 0)  // 101 & 1  -> true
            {
                Console.WriteLine("Lundi");
            }

            // teste la présence de MARDI
            if ((rdv & JourSemaine.MARDI) != 0) // 101 & 10 -> false
            {
                Console.WriteLine("Mardi");
            }
            rdv |= JourSemaine.SAMEDI; // rdv= rdv | JourSemaine.SAMEDI 101 | 100000 => 100101
            if ((rdv & JourSemaine.WEEKEND) != 0)
            {
                Console.WriteLine("WeekEnd");
            }

            // Utilisation d'une énumération avec switch
            switch (mt)
            {
                case Motorisation.ESSENCE:
                    Console.WriteLine("Essence");
                    break;
                case Motorisation.DIESEL:
                    Console.WriteLine("Diesel");
                    break;
                default:
                    Console.WriteLine("Autre motorsation");
                    break;
            }
            #endregion

            #region structure
            Point p1;
            p1.x = 3;   // accès au champs x de la structure
            p1.y = 4;
            Console.WriteLine($"x={p1.x} y={p1.y}");
            Console.WriteLine(p1);

            // Affectation d'une structure
            Point p2 = p1;
            Console.WriteLine($"x={p2.x} y={p2.y}");
            p2.x = 8;
            // Les champs de p2 sont initialisé avec les valeurs des champs de p1
            Console.WriteLine($"x={p1.x} y={p1.y}");
            Console.WriteLine($"x={p2.x} y={p2.y}");

            // Comparaison de 2 structures => Il faut comparer chaque champs
            if (p1.x == p2.x && p1.y == p2.y)
            {
                Console.WriteLine("p1 est égal à p2");
            }
            #endregion
        }
    }
}
