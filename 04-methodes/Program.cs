﻿namespace _04_methodes
{
    internal class Program
    {
        static int Main(string[] args)
        {
            // Appel de methode
            double r = Multiplier(3, 4);

            // Appel de methode (sans type retour)
            Afficher(r);

            // Exercice Maximum
            Console.WriteLine("Maximum: entrer 2 entiers:");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(Maximum(a, b));

            // Exercice Paire
            Console.WriteLine(Even(3));
            Console.WriteLine(Even(4));

            // Passage par valeur (par défaut en C#)
            // C'est une copie de la valeur du paramètre qui est transmise à la méthode
            int v = 42;
            TestParamValeur(v);
            Console.WriteLine(v); // 42

            // Passage de paramètre par référence
            //La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
            TestParamreference(ref v); // uniquement des variables
            Console.WriteLine(v); //43

            // Passage de paramètre en sortie
            // Un paramètre out ne sert à la méthode qu'à retourner une valeur
            // L'argument transmis doit référencer une variable ou un élément de tableau
            // La variable n'a pas besoin d'être initialisée
            int a1, a2;
            TestParamOut(out a1, out a2);
            Console.WriteLine($"a1={a1}, a2={a2}");

            // On peut déclarer les variables de retours dans les arguments pendant l'appel de la méthode
            TestParamOut(out int b1, out int b2);
            Console.WriteLine($"a1={b1}, a2={b2}");

            // On peut ignorer un paramètre out en le nommant _
            TestParamOut(out int c1, out _);
            Console.WriteLine($"c1={c1}");

            // Paramètre optionnel (uniquement avec le passage par valeur)
            TestParamDefault(3);                // 3 azerty a
            TestParamDefault(5, "test");        // 5 test a
            TestParamDefault(5, "hello", 'z');  // 5 hello z

            // Paramètres nommés
            TestParamDefault(v: 10, c: 'e');
            TestParamDefault(c: 'e', v: 10, str: "wxcv");
            TestParamDefault(10, c: 'e');

            // Exercice Echange
            int va = 10;
            int vb = 50;
            Console.WriteLine($"va={va} vb={vb}");
            Swap(ref va, ref vb);
            Console.WriteLine($"va={va} vb={vb}");

            // Nombre de paramètres variable
            Console.WriteLine(Moyenne(10, 20, 18, 15));
            Console.WriteLine(Moyenne(10));
            int[] notes = { 20, 18, 15 };
            Console.WriteLine(Moyenne(14, notes)); // on peut aussi passer un tableau

            // Surcharge de méthode
            // Correspondance exacte des type des paramètres
            Console.WriteLine(Somme(1, 4));     // => appel de la méthode avec 2 int en paramètres
            Console.WriteLine(Somme(1.5, 4.2)); // => appel de la méthode avec 2 double en paramètres
            Console.WriteLine(Somme(1.5, 4));   // => appel de la méthode avec 1 double et 1 entier en paramètre
            Console.WriteLine(Somme("hello", "world")); // => appel de la méthode avec 2 chaines en paramètre

            // Pas de correpondance exacte => cherche d'une méthode qui correspond avec une valeur par défaut
            Console.WriteLine(Somme(1, 4, 4));   // => appel de la méthode avec 3 int + un entier avec une valeur par défaut

            // Pas de correspondance exacte => convertion automatique
            Console.WriteLine(Somme(1L, 4L));   // => appel de la méthode avec 2 double en paramètres
            Console.WriteLine(Somme('a', 'b')); // => appel de la méthode avec 2 int en paramètres
            Console.WriteLine(Somme(4, 1.5));   // => appel de la méthode avec 2 double en paramètres

            // Pas de conversion possible => Erreur de commpilation
            // Console.WriteLine(Somme(4.4M, 1.5M));


            // Paramètre de la méthode Main
            // En lignes de commandes: 04 - methode.exe Azerty 12234 "aze rty"
            // Dans visual studio: propriétés du projet->onglet Déboguer->options de démarrage -> Arguments de la ligne de commande
            foreach (var s in args)
            {
                Console.WriteLine(s);
            }

            // Méthode récursive
            int re = Factorial(3);
            Console.WriteLine($"factoriel de 3={re}");

            // Corps d'expression
            AfficherResultat(Soustraction(4, 2));

            // Exercice Tableau
            Menu();
            return 0;
        }

        #region Déclaration de méthode
        static double Multiplier(double d1, double d2)
        {
            return d1 * d2; // L'instruction return
                            // - Interrompt l'exécution de la méthode
                            // - Retourne la valeur de l'expression à droite
        }

        static void Afficher(double val)     // void => pas de valeur retourné
        {
            Console.WriteLine(val);
            // avec void => return; ou return peut être omis ;
        }
        #endregion

        #region Exercice Maximum
        // Ecrire une méthode Maximum qui prends en paramètre 2 nombres et elle retourne le maximum
        static int Maximum(int val1, int val2)
        {
            return val1 < val2 ? val2 : val1;
            // ou
            //if (val1 < val2)
            //{
            //    return val2;
            //}
            //else
            //{
            //    return val1;
            //}
        }
        #endregion

        #region Exercice Méthode Paire
        // Écrire une méthode Paire qui prend un entier en paramètre un entier
        // Elle retourne vrai, si il est paire
        static bool Even(int val)
        {
            return val % 2 == 0;
            // ou
            // return (val & 0x1) == 0;
            // ou
            //if (val % 2 == 0)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
        }
        #endregion

        #region Passage de paramètre

        // Passage par valeur
        static void TestParamValeur(int val)
        {
            Console.WriteLine(val);
            val++;  // La modification de la valeur du paramètre a n'a pas d'influence en dehors de la méthode
            Console.WriteLine(val);
        }

        // Passage par référence => ref
        static void TestParamreference(ref int val)
        {
            Console.WriteLine(val);
            val++;
            Console.WriteLine(val);
        }

        // Passage de paramètre en sortie => out
        static void TestParamOut(out int v1, out int v2)
        {
            // Console.WriteLine(v1); // erreur, on ne peut pas accéder en entrée à un paramètre out 
            // int b = v1 * 2;
            v1 = 12;        // La méthode doit obligatoirement affecter une valeur aux paramètres out
            v2 = 2 * v1;    // une fois que l'on à donner une valeur au paramètre on peut l'utiliser

        }

        // On peut donner une valeur par défaut aux arguments passés par valeur
        static void TestParamDefault(int v, string str = "azerty", char c = 'a')
        {
            Console.WriteLine($"{v} {str} {c}");
        }
        #endregion
        #region Exercice Echange
        // Écrire une méthode Swap qui prend en paramètre 2 entiers et qui permet d'inverser le contenu des 2 variables passées en paramètre
        static void Swap(ref int v1, ref int v2)
        {
            int tmp = v1;
            v1 = v2;
            v2 = tmp;
        }
        #endregion

        #region Nombre d'arguments variable => params
        static double Moyenne(int val, params int[] valeurs)
        {
            double somme = val;
            foreach (var elm in valeurs)
            {
                somme += elm;
            }
            return somme / (valeurs.Length + 1);
        }
        #endregion

        #region Surcharge de méthode
        // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même nom, mais leurs signatures doient être différentes
        // La signature d'une méthode correspond aux types et nombre de paramètres
        // Le type de retour ne fait pas partie de la signature
        static int Somme(int a, int b)
        {
            Console.WriteLine("2 entiers");
            return a + b;
        }
        //static int Somme(int i1, int i2) // Erreur de compliation -> 2 méthodes ont le même type et le même nombre de paramètre 
        //{
        //    return i1 + i2;
        //}

        static double Somme(double a, double b)
        {
            Console.WriteLine("2 double");
            return a + b;
        }

        static double Somme(double a, int b)
        {
            Console.WriteLine("1 double 1 entiers");
            return a + b;
        }

        static string Somme(string s1, string s2)
        {
            Console.WriteLine("2 chaines de caractères");
            return s1 + s2;
        }

        static double Somme(int a, int b, int c, int d = 3)
        {
            Console.WriteLine("3 entiers et un entier avec une valeur par défaut");
            return a + b + c + d;
        }
        #endregion
        #region méthode récurcive
        // Récursivité: une méthode qui s'appelle elle-même
        static int Factorial(int n)
        {
            if (n <= 1)// condition de sortie
            {
                return 1;
            }
            else
            {
                return Factorial(n - 1) * n;
            }
        }
        #endregion

        #region corps d'expression
        // une méthode qui comprend une seule expression, peut s'écrire avec l'opérateur =>

        static int Soustraction(int a, int b) => a - b;
        /*  {
              return a - b;
          }*/

        static void AfficherResultat(int v) => Console.WriteLine(v);
        #endregion
        #region Exercice Tableau
        // - Écrire une méthode qui affiche un tableau d’entier

        // - Écrire une méthode qui permet de saisir :
        //   - La taille du tableau
        //   - Les éléments du tableau

        // - Écrire une méthode qui calcule :
        //   - le maximum
        //   - la moyenne

        // - Faire un menu qui permet de lancer ces méthodes
        //      1 - Saisir le tableau
        //      2 - Afficher le tableau
        //      3 - Afficher le maximum et la moyenne
        //      0 - Quitter
        //      Choix =
        static void AfficherTab(int[] tab)
        {
            Console.Write("[ ");
            foreach (var elm in tab)
            {
                Console.Write($"{elm} ");
            }
            Console.WriteLine("]");
        }

        static int[] SaisirTab()
        {
            Console.WriteLine("Saisir le nombre d'élément du tableau");
            int size = Convert.ToInt32(Console.ReadLine());
            int[] tabInt = new int[size];
            for (int i = 0; i < size; i++)
            {
                Console.Write($"tabInt[{i}]= ");
                tabInt[i] = Convert.ToInt32(Console.ReadLine());
            }
            return tabInt;
        }

        static void CalculTab(int[] tab, out int maximum, out double moyenne)
        {
            maximum = tab[0];
            double somme = 0.0;
            foreach (var v in tab)
            {
                if (v > maximum)
                {
                    maximum = v;
                }
                somme += v;
            }
            moyenne = somme / tab.Length;
        }

        static void AfficherMenu()
        {
            Console.WriteLine("\n1 - Saisir le tableau");
            Console.WriteLine("2 - Afficher le tableau");
            Console.WriteLine("3 - Afficher le maximum et la moyenne");
            Console.WriteLine("0 - Quitter");
        }

        static void Menu()
        {
            int[] t = null;
            AfficherMenu();
            int choix;
            do
            {
                Console.Write("choix= ");
                choix = Convert.ToInt32(Console.ReadLine());
                switch (choix)
                {
                    case 1:
                        t = SaisirTab();
                        break;
                    case 2:
                        if (t != null)
                        {
                            AfficherTab(t);
                        }
                        break;
                    case 3:
                        if (t != null)
                        {
                            CalculTab(t, out int max, out double moyenne);
                            Console.WriteLine($"Maximum={max} Moyenne={moyenne}");
                        }
                        break;
                    case 0:
                        Console.WriteLine("au revoir !");
                        break;
                    default:
                        AfficherMenu();
                        Console.WriteLine($"Le choix {choix} n'existe pas");
                        break;
                }
            }
            while (choix != 0);
            #endregion
        }

    }
}