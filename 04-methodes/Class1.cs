﻿namespace _04_methodes
{
    internal class Class1
    {
        // Dans un programme, il ne peut y avoir qu’une seule classe contenant une méthode Main.
        // Dans le cas contraire, il faut compiler avec l’option -main pour préciser le main utilisé
        // ou avec Visual Studio Propriété->Application-> choisir l'objet de démarrage

        static void Main()
        {
            Console.WriteLine("Autre main");
        }
    }
}
